static  int      myDarkRed     = TColor::GetColor(128,0,0);
static  int      myDarkGreen   = TColor::GetColor(0,128,0);
static  int      myDarkBlue    = TColor::GetColor(0,0,128);

Int_t fillColor[5] = {kAzure-9,kOrange-9,kGray,kGreen-10,kRed-10};
Int_t markColor[7] = {kAzure-6,kOrange+7,1,kGreen+3,kRed+1,myDarkGreen,kRed+1};
Int_t markStyle[5] = {20,21,33,34,24};


void plotComparePbPbToPPbMean(TString strPbPb = "MeanJetMass_Area.root", TString strpPb = "MeanJetMass_Area.root", TString strLegPbPb = "Pb-Pb 0-10%", TString strLegPPb = "p-Pb MB") {

  //Pb-Pb: /data/wrk/JetMass/Data/LHC11h/MergeGoodSemiGood/Trains740_741_742/unfoldRM746/Area/MeanJetMass_Area.root

  TString strPyt = "/media/data/wrk/JetMass/ToyModel/pdsf/PYTHIAP11/1410100105/JetMass.root";
  TString strJew = "/data/Dropbox/jet_shapes/Marta/JetMassJEWELR040.root";
  Bool_t bDrawPyt = kFALSE;//kTRUE;
  Bool_t bDrawQ   = kFALSE;//kTRUE;

  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  gROOT->LoadMacro("$gitJetMass/utils/plotUtils.C");
  gROOT->LoadMacro("$gitJetMass/utils/style.C");
  SetStyle(0);

  const Int_t nFiles = 2;
  TString strLeg[nFiles] = {strLegPbPb,strLegPPb};
  TString strEcms[nFiles] = {"#sqrt{#it{s}_{NN}}=2.76 TeV","#sqrt{#it{s}_{NN}}=5.02 TeV"};
  TFile *f[nFiles] = {0};
  f[0] = new TFile(strPbPb.Data());
  f[1] = new TFile(strpPb.Data());

  const Int_t nPtBins = 5;
  Double_t ptmin[nPtBins] = {20.,40.,60.,80.,100.};
  Double_t ptmax[nPtBins] = {40.,60.,80.,100.,120.};
  //  Double_t diffEcms[nPtBins] = {0.143028,0.268195,0.398801,0.606928,0.772570};
  //Double_t diffEcms[nPtBins] = {0.00423365,0.196278,0.359701,0.488692};
  //Double_t diffEcms[nPtBins] = {2.76040000000000063e-01,3.68309999999999249e-01,5.80999999999999517e-01,6.57399999999999096e-01};
  Double_t diffEcms[nPtBins] = {0.136080,0.192040,0.270260,0.354760,0.528700};
  TGraphAsymmErrors *grDiffEcms = new TGraphAsymmErrors();

  TGraphErrors *grUnf[nFiles];
  TGraphErrors *grSys[nFiles];

  for(Int_t i = 0; i<nFiles; i++) {
    grUnf[i] = dynamic_cast<TGraphErrors*>f[i]->Get("grUnfMean");
    grSys[i] = dynamic_cast<TGraphErrors*>f[i]->Get("grUnfMeanSyst");
    if(!grSys[i])
      grSys[i] = dynamic_cast<TGraphErrors*>f[i]->Get("grSyst2");
  }

  TCanvas *c8 = new TCanvas("c8","c8: mean mass with systematic",700,630);
  TH1F *fr8 = = DrawFrame(ptmin[2]-2.,120.+2.,0.,22.,"#it{p}_{T,ch jet} (GeV/#it{c})","#LT#it{M}_{ch jet}#GT (GeV/#it{c}^{2})");
  // TLegend *leg8 = CreateLegend(0.21,0.93,0.17,0.35,"",0.05);
  // leg8->SetNColumns(2);
  TLegend *leg8 = CreateLegend(0.23,0.5,0.63,0.93,"",0.05);
  for(Int_t k = 0; k<nFiles; k++) {
    if(k==1) {
      grUnf[k]->Print();
      for(Int_t i = 0; i<grUnf[k]->GetN(); i++) {
        Double_t x,y;
        grUnf[k]->GetPoint(i,x,y);
        grDiffEcms->SetPoint(i,x,y);
        grDiffEcms->SetPointError(i,4.,4.,diffEcms[i],0.);
      }
      grDiffEcms->SetFillColor(fillColor[k]);
      grDiffEcms->SetMarkerColor(markColor[k]);
      grDiffEcms->SetLineColor(0);//markColor[k]);
      grDiffEcms->SetLineWidth(2);
      grDiffEcms->SetMarkerStyle(markStyle[k]);
      grDiffEcms->Draw("2");
      grDiffEcms->Print();
    }

    grSys[k]->SetFillColor(fillColor[k]);
    if(k==1) grSys[k]->SetFillStyle(0);
    grSys[k]->SetMarkerColor(markColor[k]);
    grSys[k]->SetLineColor(markColor[k]);
    if(k==0) grSys[k]->SetLineColor(0);
    grSys[k]->SetLineWidth(2);
    grSys[k]->SetMarkerStyle(markStyle[k]);
    grSys[k]->Draw("2");
  }
  for(Int_t k = 0; k<nFiles; k++) {
    grUnf[k]->SetMarkerColor(markColor[k]);
    grUnf[k]->SetLineColor(markColor[k]);
    grUnf[k]->SetMarkerStyle(markStyle[k]);
    grUnf[k]->SetMarkerSize(1.5);
    //  if(k==1) grUnf[k]->SetMarkerSize(1.2);
    grUnf[k]->SetLineWidth(3);
    grUnf[k]->Draw("pz");
    leg8->AddEntry(grUnf[k],Form("#splitline{%s}{%s}",strLeg[k].Data(),strEcms[k].Data()),"p");

  }
   grUnf[0]->Draw("pz");

  if(bDrawPyt) {
    TFile *fPyt = new TFile(strPyt.Data());
    TGraphErrors *grMeanPyt = (TGraphErrors*)fPyt->Get("grMeanMass");
    grMeanPyt->SetLineWidth(2);
    grMeanPyt->SetLineColor(1);
    grMeanPyt->SetMarkerColor(1);
    grMeanPyt->SetMarkerStyle(24);
    grMeanPyt->SetMarkerSize(1.3);
    grMeanPyt->Draw("pz");
    leg8->AddEntry(grMeanPyt,"PYTHIA","p");
  }

  if(bDrawQ) {
     TFile *fJEWEL = new TFile(strJew.Data());
     TString strLegQ[2] = {"JEWEL","QPYTHIA"};
     TGraphErrors *grMeanJEWEL[2];
     grMeanJEWEL[0] = dynamic_cast<TGraphErrors*>fJEWEL->Get("grMeanMass_JEWEL RecOff");
     grMeanJEWEL[1] = dynamic_cast<TGraphErrors*>fJEWEL->Get("grMeanMass_QPYTHIA");
     for(Int_t t = 0; t<2; t++) {
       grMeanJEWEL[t]->SetLineWidth(2);
       grMeanJEWEL[t]->SetLineColor(markColor[t+5]);
       grMeanJEWEL[t]->SetMarkerColor(markColor[t+5]);
       grMeanJEWEL[t]->SetMarkerStyle(28);
       grMeanJEWEL[t]->SetMarkerSize(1.5);
       grMeanJEWEL[t]->Draw("pz");
       leg8->AddEntry(grMeanJEWEL[t],Form("%s",strLegQ[t].Data()),"p");
     }
  }

  leg8->Draw();
  TLegend *leg8b = CreateLegend(0.62,0.92,0.83,0.9,"",0.05);
  leg8b->AddEntry(grSys[0],"Systematic","f");
  leg8b->Draw();
  TLegend *leg8c = CreateLegend(0.62,0.92,0.65,0.76,"",0.05);
  leg8c->AddEntry(grSys[1],"Systematic","f");
  leg8c->AddEntry(grDiffEcms,"#sqrt{#it{s}} difference","f");
  leg8c->Draw();

  //  DrawLatex(0.25,0.83,"Charged jets; Anti-#it{k}_{T} #it{R}=0.4");
  Double_t xminleg = 0.27;
  Double_t ymaxleg = 0.33;//0.88;
  Double_t ydiffleg = 0.07;
  DrawLatex(xminleg,ymaxleg,"ALICE");
  DrawLatex(xminleg,ymaxleg-ydiffleg,"Charged jets");
  DrawLatex(xminleg,ymaxleg-2.*ydiffleg,"Anti-#it{k}_{T} #it{R}=0.4");

  c8->SaveAs("MeanJetMassPbPbVsPPb.png");  
  c8->SaveAs("MeanJetMassPbPbVsPPb.eps");

}
