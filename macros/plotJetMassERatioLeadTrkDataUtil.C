Int_t colors[7] = {1,kGreen+2,kBlue,kCyan,kRed,kMagenta,kYellow+1};
const Bool_t bNorm = kFALSE;//kTRUE;

void plotJetMassERatioLeadTrkDataUtil(TString strf = "AnalysisResults.root", Double_t R = 0.4, Double_t ptmin = 60., Double_t ptmax = 80., TString type = "Charged", Int_t centBin = 0) {

  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  gROOT->LoadMacro("$gitJetMass/utils/plotUtils.C");
  gROOT->LoadMacro("$gitJetMass/utils/style.C");
  SetStyle(0);
 
  Double_t centMin[4] = {0.,10.,30.,50.};
  Double_t centMax[4] = {10.,30.,50.,80.};

  TString strLeg[3] = {"Area based","Raw","Constituent"};
  //TString strLeg[nLists] = {"Raw","Raw","Constituent"};
  TString strCent[4] = {"0-10%","10-30%","30-50%","50-80%"};

  const Int_t nLeadTrkBins = 6;
  Double_t leadTrkPt[nLeadTrkBins] = {0.,2.,6.,10.,15.,19.};

  TString outName = Form("JetMassERatioCent%dR%03dPtMin%.0fPtMax%.0fType%s.root",centBin,(Int_t)(R*100.),ptmin,ptmax,type.Data());

  Int_t available[3] = {0,0,0};
  gROOT->LoadMacro("$gitJetMass/classes/PlotUtilEmcalJetMass.cxx+");
  PlotUtilEmcalJetMass *util = new PlotUtilEmcalJetMass();
  util->SetInputFileName(strf);
  util->SetJetRadius(R);
  util->SetJetType(type);
  util->SetCentBin(centBin);
  util->SetJetPtRange(ptmin,ptmax);

  util->LoadFile();

  util->SetTag("");
  util->SetConstTag("");
  if(util->LoadList()) available[0] = 1;

  util->SetTag("Raw");
  util->SetConstTag("");
  if(util->LoadList()) available[1] = 1;

  util->SetTag("Raw");
  util->SetConstTag("ConstSub");
  if(util->LoadList()) available[2] = 1;

  Printf("Loaded lists");

  TH1D *hMERatioAll[3][nLeadTrkBins];
  TH1D *hMERatioTagged[3][nLeadTrkBins];
  TH1D *hMERatioTaggedMatch[3][nLeadTrkBins];
  TGraphErrors *grMeanMassERatioPtLeadTrAll[3];
  TGraphErrors *grMeanMassERatioPtLeadTrTagged[3];
  TGraphErrors *grMeanMassERatioPtLeadTrTaggedMatch[3];
  for(Int_t i = 0; i<3; i++) {
    grMeanMassERatioPtLeadTrAll[i] = new TGraphErrors();
    grMeanMassERatioPtLeadTrTagged[i] = new TGraphErrors();
    grMeanMassERatioPtLeadTrTaggedMatch[i] = new TGraphErrors();
  }
  for(Int_t j = 0; j<nLeadTrkBins; j++) {
    util->SetMinLeadTrackPt(leadTrkPt[j]);
    for(Int_t i = 0; i<3; i++) {
      if(available[i]==0) {
	Printf("Skipping list %d",i);
	continue;
      }
      hMERatioAll[i][j] = util->GetJetMassERatioDistribution(PlotUtilEmcalJetMass::kJetAll,i);
      hMERatioTagged[i][j] = util->GetJetMassERatioDistribution(PlotUtilEmcalJetMass::kJetTagged,i);
      hMERatioTaggedMatch[i][j] = util->GetJetMassERatioDistribution(PlotUtilEmcalJetMass::kJetTaggedMatch,i);
      grMeanMassERatioPtLeadTrAll[i]->SetPoint(grMeanMassERatioPtLeadTrAll[i]->GetN(),leadTrkPt[j],hMERatioAll[i][j]->GetMean());
      grMeanMassERatioPtLeadTrTagged[i]->SetPoint(grMeanMassERatioPtLeadTrTagged[i]->GetN(),leadTrkPt[j],hMERatioTagged[i][j]->GetMean());
      grMeanMassERatioPtLeadTrTaggedMatch[i]->SetPoint(grMeanMassERatioPtLeadTrTaggedMatch[i]->GetN(),leadTrkPt[j],hMERatioTaggedMatch[i][j]->GetMean());

      grMeanMassERatioPtLeadTrAll[i]->SetPointError(grMeanMassERatioPtLeadTrAll[i]->GetN()-1,0.,hMERatioAll[i][j]->GetMeanError());
      grMeanMassERatioPtLeadTrTagged[i]->SetPointError(grMeanMassERatioPtLeadTrTagged[i]->GetN()-1,0.,hMERatioTagged[i][j]->GetMeanError());
      grMeanMassERatioPtLeadTrTaggedMatch[i]->SetPointError(grMeanMassERatioPtLeadTrTaggedMatch[i]->GetN()-1,0.,hMERatioTaggedMatch[i][j]->GetMeanError());
    }
  }

  Printf("Got all histos");

  Double_t mmax = 0.8;
  if(R==0.2) mmax = 0.8;
  if(R==0.4) mmax = 0.8;

  TCanvas *c1 =new TCanvas("c1","c1: jet mass",900,700);
  c1->Divide(2,2);
  TH1F *frame[3];
  TLegend *leg1[3];
  for(Int_t i = 0; i<3; i++) {
    if(available[i]==0) continue;
    c1->cd(i+1);
    frame[i] = DrawFrame(-0.2,mmax,0.,0.3,"#it{M}_{jet}/#it{E}_{jet}","#frac{#it{N}_{jet}(#it{p}_{T,lead trk}>X)}{#it{N}_{jet}(all)}");
    leg1[i] = CreateLegend(0.25,0.6,0.52,0.94,strLeg[i].Data());
    leg1[i]->SetTextSize(leg1[i]->GetTextSize()*0.8);
    Double_t norm = 0.;
    for(Int_t nt = 0; nt<nLeadTrkBins; nt++) {
      if(nt==0) norm = hMERatioAll[i][nt]->Integral();
      if(norm>0.) {
	//Printf("norm: %f",norm);
	hMERatioAll[i][nt]->Scale(1./norm);

	hMERatioAll[i][nt]->SetLineColor(colors[nt]);
	hMERatioAll[i][nt]->SetMarkerColor(hMERatioAll[i][nt]->GetLineColor());
	hMERatioAll[i][nt]->SetMarkerStyle(20);
	
	hMERatioAll[i][nt]->DrawCopy("same");

	hMERatioTagged[i][nt]->Scale(1./norm);
	hMERatioTagged[i][nt]->SetLineColor(colors[nt]);
	hMERatioTagged[i][nt]->SetMarkerColor(hMERatioTagged[i][nt]->GetLineColor());
	hMERatioTagged[i][nt]->SetMarkerStyle(24);
	
	hMERatioTagged[i][nt]->DrawCopy("same");

	hMERatioTaggedMatch[i][nt]->Scale(1./norm);
	hMERatioTaggedMatch[i][nt]->SetLineColor(colors[nt]);
	hMERatioTaggedMatch[i][nt]->SetMarkerColor(hMERatioTaggedMatch[i][nt]->GetLineColor());
	hMERatioTaggedMatch[i][nt]->SetMarkerStyle(27);
	
	hMERatioTaggedMatch[i][nt]->DrawCopy("same");
      
	leg1[i]->AddEntry(hMERatioTagged[i][nt],Form("#it{p}_{T,lead trk}=%.0f #LT#it{M}_{jet}#GT=%.1f RMS=%.1f",leadTrkPt[nt],hMERatioTagged[i][nt]->GetMean(),hMERatioTagged[i][nt]->GetRMS()),"p");
      }
    }
  }

  for(Int_t i = 0; i<3; i++) {
    if(available[i]==0) continue;
    c1->cd(i+1);
    leg1[i]->Draw();
  }

  c1->cd(4);
  TH1F *frame4 = DrawFrame(-0.5,20.5,0.16,0.3,"#it{p}_{T,lead track}","#LT#it{M}_{jet}#GT");
  for(Int_t i = 0; i<1; i++) {
    if(available[i]==0) continue;
    grMeanMassERatioPtLeadTrAll[i]->SetMarkerColor(colors[i]);
    grMeanMassERatioPtLeadTrAll[i]->SetMarkerStyle(20);
    grMeanMassERatioPtLeadTrAll[i]->Draw("PL");

    grMeanMassERatioPtLeadTrTagged[i]->SetMarkerColor(colors[i]);
    grMeanMassERatioPtLeadTrTagged[i]->SetMarkerStyle(24);
    grMeanMassERatioPtLeadTrTagged[i]->Draw("PL");

    grMeanMassERatioPtLeadTrTaggedMatch[i]->SetMarkerColor(colors[i]);
    grMeanMassERatioPtLeadTrTaggedMatch[i]->SetMarkerStyle(27);
    grMeanMassERatioPtLeadTrTaggedMatch[i]->Draw("PL");
  }

  c1->SaveAs(Form("MassERatioLeadTrkPtBinVsCent%d_%s.png",centBin,type.Data()));


  TFile *fout = new TFile(outName.Data(),"RECREATE");
  for(Int_t i = 0; i<3; i++) {
    if(available[i]==0) continue;
    grMeanMassERatioPtLeadTrAll[i]->Write(Form("grMeanMassERatioPtLeadTrAll_List%d",i));
    grMeanMassERatioPtLeadTrTagged[i]->Write(Form("grMeanMassERatioPtLeadTrTagged_List%d",i));
    grMeanMassERatioPtLeadTrTaggedMatch[i]->Write(Form("grMeanMassERatioPtLeadTrTaggedMatch_List%d",i));

    for(Int_t nt = 0; nt<nLeadTrkBins; nt++) {
      hMERatioAll[i][nt]->Write();
      hMERatioTagged[i][nt]->Write();
      hMERatioTaggedMatch[i][nt]->Write();
    }
  }
}
