Int_t colors[7] = {1,kRed,kBlue,kCyan,kGreen+2,kMagenta,kYellow+1};

void plotCompareMeanMassDistUtil(TString baseDir1, TString baseDir2, Int_t centBin = 0, Double_t R, Double_t maxm = 12., Double_t leadTrkPt = 0., Double_t ptmin = 60., Double_t ptmax = 80., TString type = "Charged", TString strLeg1 = "PYTHIA embedded", TString strLeg2 = "Data") {

  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  gROOT->LoadMacro("$gitJetMass/utils/plotUtils.C");
  gROOT->LoadMacro("$gitJetMass/utils/style.C");
  gROOT->LoadMacro("$gitJetMass/utils/utilsMV.C");
  SetStyle(0);

  TString str1 = Form("%s/JetMassCent%dR%03dPtMin%.0fPtMax%.0fType%s.root",baseDir1.Data(),centBin,(Int_t)(R*100.),ptmin,ptmax,type.Data());
  TString str2 = Form("%s/JetMassCent%dR%03dPtMin%.0fPtMax%.0fType%s.root",baseDir2.Data(),centBin,(Int_t)(R*100.),ptmin,ptmax,type.Data());

  TFile *f[2];
  f[0] = new TFile(str1.Data());
  f[1] = new TFile(str2.Data());

  f[0]->ls();
  f[1]->ls();
  Int_t nFiles = 2;
  if(str2.IsNull()) nFiles = 1;
  Printf("nFiles: %d",nFiles);

  Int_t centMin[4] = {0,10,30,50};
  Int_t centMax[4] = {10,30,50,80};

  TString strLeg[2] = {strLeg1,strLeg2};

  Double_t mmin = 0.;
  Double_t mmax = 20.;
  if(ptmin==20.) {
    mmin = 2.; 
    mmax = 10.;
  }
  else if(ptmin==40.) {
    mmin = 6.; 
    mmax = 15.;
  } else if(ptmin==60.) {
    mmin = 6.; 
    mmax = 18.;
  } else if(ptmin==80.) {
    mmin = 10.; 
    mmax = 20.;
  }
  mmin = 6.; 
  mmax = 18.;

  Int_t nListsPlot = 3;
  const Int_t nLists = 4; // 0 = area based; 1 = raw; 2 = constituent; 3 = EmbOnly
  TString strLists[nLists] = {"Area based","Raw","Constituent","EmbOnly"};
  TGraphErrors *grMeanMassTagged[nLists][2];
  TH1 *hMAllPtBin[nLists][2];
  for(Int_t j = 0; j<nFiles; j++) {
    for(Int_t i = 0; i<nLists; i++) { 
      TString histName = Form("grMeanPtLeadTrTagged_List%d",i);
      if(j==0) 
        histName = Form("grMeanPtLeadTrTaggedMatch_List%d",i);
        if(i==3) histName = Form("grMeanPtLeadTrAll_List%d",i);
      grMeanMassTagged[i][j] = (TGraphErrors*)f[j]->Get(histName.Data());
    }
  }

  TCanvas *c1 =new TCanvas("c1","c1",430,400);
  TH1F *fr1 = = DrawFrame(-0.5,20.,mmin,mmax,"#it{p}_{T, lead track}^{min}","#LT#it{M}_{jet}#GT");
  TString tmp = Form("%d-%d",centMin[centBin],centMax[centBin]);
  tmp+="%;";
  tmp+=Form(" %.0f<#it{p}_{T,jet}<%.0f; #it{R}=%.1f",ptmin,ptmax,R);
  //  cout << tmp << endl;
  // break;
  TLegend *leg1 = CreateLegend(0.25,0.6,0.61,0.93,tmp.Data(),0.05);
  for(Int_t i = 0; i<nListsPlot; i++) {
    if(i==1) continue; 
    // if(i<3) continue;
    for(Int_t j = 0; j<nFiles; j++) {
      if(grMeanMassTagged[i][j]) {
        grMeanMassTagged[i][j]->SetLineWidth(2);
        grMeanMassTagged[i][j]->SetLineColor(colors[j]);
        grMeanMassTagged[i][j]->SetMarkerColor(colors[j]);
        grMeanMassTagged[i][j]->SetMarkerStyle(GetMarker(i));
        grMeanMassTagged[i][j]->Draw("pzl");
        leg1->AddEntry(grMeanMassTagged[i][j],Form("%s %s",strLeg[j].Data(),strLists[i].Data()),"p");
      } else Printf("Couldn't find graph %d %d",i,j);
     }
  }
  leg1->Draw();
  // DrawLatex(0.75,0.65,Form("#it{R}=%.1f",R),0.05);


  TString strPt = Form("PtMin%.0fPtMax%.0f",ptmin,ptmax);
  c1->SaveAs(Form("JetMeanMassCompPbPbPYTHIAEmbeddedCent%d%sR%03d.eps",centBin,strPt.Data(),(Int_t)(R*100.)));
  c1->SaveAs(Form("JetMeanMassCompPbPbPYTHIAEmbeddedCent%d%sR%03d.png",centBin,strPt.Data(),(Int_t)(R*100.)));

}
