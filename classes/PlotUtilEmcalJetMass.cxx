#include "PlotUtilEmcalJetMass.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMath.h"
#include "Riostream.h"
#include "TH1.h"
#include "TRandom.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "THnSparse.h"
#include "TArrayF.h"
#include "TArrayD.h"
#include <TRandom3.h>
#include "TLegend.h"
#include "TLatex.h"
#include "TLine.h"
#include "TVirtualFitter.h"
#include "TList.h"

#define round(x) ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))

using std::cout;
using std::endl;

ClassImp(PlotUtilEmcalJetMass)

//----------------------------------------------------------------------------
PlotUtilEmcalJetMass::PlotUtilEmcalJetMass():
  fFileInName(0),
  fFileIn(0),
  fListArr(0),
  fRadius(0.4),
  fPtMin(60.),
  fPtMax(80.),
  fSuffix(""),
  fJetType("Charged"),
  fTag(""),
  fConstTag(""),
  fCentBin(0),
  fMinLeadTrkPt(-1.),
  fMaxLeadTrkPt(1e6)
{
  fListArr.SetOwner(kTRUE);
}

//----------------------------------------------------------------------------
Bool_t PlotUtilEmcalJetMass::LoadFile() {

  if(!fFileInName.IsNull())
    fFileIn = new TFile(fFileInName.Data());
  else {
    Printf("No file name provided: %s",fFileInName.Data());
    return kFALSE;
  }
  fFileIn->ls();
  // if(!LoadList()) return kFALSE;

  return kTRUE;
}

//----------------------------------------------------------------------------
Bool_t PlotUtilEmcalJetMass::LoadList() {

  if(!fFileIn) {
    Printf("Cannot load list without file.");
    return kFALSE;
  }

  TString strTrk = "PicoTracks_pT0150";
  if(fJetType.Contains("MCParticlesSelected")) strTrk = "MCParticlesSelected_pT0000";
  TString strClus = "";
  if(fJetType.Contains("Full")) strClus = "CaloClustersCorr";
  TString strJetType = "Charged";
  if(fJetType.Contains("Full")) strJetType = "Full";
  TString listName = Form("JetMass_Jet%s_AKT%sR%03d_%s_%sE_scheme%s_TC%s",fSuffix.Data(),strJetType.Data(),(Int_t)(fRadius*100),strTrk.Data(),strClus.Data(),fConstTag.Data(),fTag.Data());
  //  Printf("listName: %s",listName.Data());
  TList *lst = dynamic_cast<TList*>(fFileIn->Get(listName.Data()));
  if(!lst) {
    Printf("Couldn't find list %s",listName.Data());
    fListArr.Add(0x0);
    return kFALSE;
  }
  fListArr.Add(lst);
  return kTRUE;
}

//----------------------------------------------------------------------------
TH1D* PlotUtilEmcalJetMass::GetJetMassDistribution(JetSelection jSel, Int_t iList) {

  TString strJSel = "";
  if(jSel==kJetAll) strJSel = "AllSel";
  else if(jSel==kJetTagged) strJSel = "Tagged";
  else if(jSel==kJetTaggedMatch) strJSel = "TaggedMatch";
  else {
    Printf("Jet selection unknown. Returning null pointer.");
    return 0x0;
  }

  TList *lst = static_cast<TList*>(fListArr.At(iList));
  if(!lst){
    Printf("Couldn't find list %d",iList);
    return 0x0;
  }

  TString histName = Form("fh3PtJet1VsMassVsLeadPt%s_%d",strJSel.Data(),fCentBin);
  TH3 *fh3PtVsMassVsLeadPtJet1 = dynamic_cast<TH3*>(lst->FindObject(histName.Data()));
  if(!fh3PtVsMassVsLeadPtJet1) {
    Printf("Couldn't find fh3PtVsMassVsLeadPtJet1 %s. Returning null pointer.",histName.Data());
    return 0x0;
  }
  fh3PtVsMassVsLeadPtJet1->Sumw2();

  if(!fh3PtVsMassVsLeadPtJet1->GetSumw2()) fh3PtVsMassVsLeadPtJet1->Sumw2();
  Int_t minz = fh3PtVsMassVsLeadPtJet1->GetZaxis()->FindBin(fMinLeadTrkPt+0.000001);
  Int_t maxz = fh3PtVsMassVsLeadPtJet1->GetZaxis()->FindBin(fMaxLeadTrkPt-0.000001);
  Int_t minx = fh3PtVsMassVsLeadPtJet1->GetXaxis()->FindBin(fPtMin+0.00001);
  Int_t maxx = fh3PtVsMassVsLeadPtJet1->GetXaxis()->FindBin(fPtMax-0.00001);
  TH1D *hM = dynamic_cast<TH1D*>(fh3PtVsMassVsLeadPtJet1->ProjectionY(Form("fh1Mass%s_Lst%d_Cent%d_LeadTrk%.0f",strJSel.Data(),iList,fCentBin,fMinLeadTrkPt),minx,maxx,minz,maxz));
  return hM;
}

//----------------------------------------------------------------------------
TH1D* PlotUtilEmcalJetMass::GetJetMassERatioDistribution(JetSelection jSel, Int_t iList) {

  TString strJSel = "";
  if(jSel==kJetAll) strJSel = "AllSel";
  else if(jSel==kJetTagged) strJSel = "Tagged";
  else if(jSel==kJetTaggedMatch) strJSel = "TaggedMatch";
  else {
    Printf("Jet selection unknown. Returning null pointer.");
    return 0x0;
  }

  TList *lst = static_cast<TList*>(fListArr.At(iList));
  if(!lst){
    Printf("Couldn't find list %d",iList);
    return 0x0;
  }

  TString histName = Form("fh3PtJet1VsRatVsLeadPt%s_%d",strJSel.Data(),fCentBin);
  TH3 *fh3PtVsRatVsLeadPtJet1 = dynamic_cast<TH3*>(lst->FindObject(histName.Data()));
  if(!fh3PtVsRatVsLeadPtJet1) {
    Printf("Couldn't find fh3PtVsRatVsLeadPtJet1 %s. Returning null pointer.",histName.Data());
    return 0x0;
  }

  fh3PtVsRatVsLeadPtJet1->Sumw2();
  Int_t minz = fh3PtVsRatVsLeadPtJet1->GetZaxis()->FindBin(fMinLeadTrkPt+0.000001);
  Int_t maxz = fh3PtVsRatVsLeadPtJet1->GetZaxis()->FindBin(fMaxLeadTrkPt-0.000001);
  Int_t minx = fh3PtVsRatVsLeadPtJet1->GetXaxis()->FindBin(fPtMin+0.00001);
  Int_t maxx = fh3PtVsRatVsLeadPtJet1->GetXaxis()->FindBin(fPtMax-0.00001);
  TH1D *hMERatio = dynamic_cast<TH1D*>(fh3PtVsRatVsLeadPtJet1->ProjectionY(Form("fh1MassOverE%s_Lst%d_Cent%d_LeadTrk%.0f",strJSel.Data(),iList,fCentBin,fMinLeadTrkPt),minx,maxx,minz,maxz));
  return hMERatio;
}

//----------------------------------------------------------------------------
TH1D* PlotUtilEmcalJetMass::GetJetPtDistribution(JetSelection jSel, Int_t iList) {

  TString strJSel = "";
  if(jSel==kJetAll) strJSel = "AllSel";
  else if(jSel==kJetTagged) strJSel = "Tagged";
  else if(jSel==kJetTaggedMatch) strJSel = "TaggedMatch";
  else {
    Printf("Jet selection unknown. Returning null pointer.");
    return 0x0;
  }

  TList *lst = static_cast<TList*>(fListArr.At(iList));
  if(!lst){
    Printf("Couldn't find list %d",iList);
    return 0x0;
  }

  TString histName = Form("fh3PtJet1VsMassVsLeadPt%s_%d",strJSel.Data(),fCentBin);
  TH3 *fh3PtVsMassVsLeadPtJet1 = dynamic_cast<TH3*>(lst->FindObject(histName.Data()));
  if(!fh3PtVsMassVsLeadPtJet1) {
    Printf("Couldn't find fh3PtVsMassVsLeadPtJet1 %s. Returning null pointer.",histName.Data());
    return 0x0;
  }

  Int_t minz = fh3PtVsMassVsLeadPtJet1->GetZaxis()->FindBin(fMinLeadTrkPt+0.000001);
  Int_t maxz = fh3PtVsMassVsLeadPtJet1->GetZaxis()->FindBin(fMaxLeadTrkPt-0.000001);
  TH1D *hPt = dynamic_cast<TH1D*>(fh3PtVsMassVsLeadPtJet1->ProjectionX(Form("fh1Pt%s_Lst%d_Cent%d_LeadTrk%.0f_%.0f",strJSel.Data(),iList,fCentBin,fMinLeadTrkPt,fMaxLeadTrkPt),1,-1,minz,maxz));
  return hPt;
}

//----------------------------------------------------------------------------
TH1* PlotUtilEmcalJetMass::GetCentralityHist(Int_t iList) const {

  TList *lst = static_cast<TList*>(fListArr.At(iList));
  if(!lst){
    Printf("Couldn't find list %d",iList);
    return 0x0;
  }

  TString histName = Form("fHistCentrality");
  TH1 *fHistCentrality = dynamic_cast<TH1*>(lst->FindObject(histName.Data()));
  return fHistCentrality;
}

//----------------------------------------------------------------------------
Double_t PlotUtilEmcalJetMass::GetNEvents(Int_t iList) const {

  // TList *lst = static_cast<TList*>(fListArr.At(iList));
  // if(!lst){
  //   Printf("Couldn't find list %d",iList);
  //   return 0x0;
  // }

  // TString histName = Form("fHistCentrality");
  // TH1 *fHistCentrality = dynamic_cast<TH1*>(lst->FindObject(histName.Data()));
  TH1 *fHistCentrality = GetCentralityHist(iList);
  Double_t centMin[4] = {0.,10.,30.,50.};
  Double_t centMax[4] = {10.,30.,50.,100.};
  Int_t min = fHistCentrality->GetXaxis()->FindBin(centMin[fCentBin]+0.000001);
  Int_t max = fHistCentrality->GetXaxis()->FindBin(centMax[fCentBin]-0.000001);
  Double_t nEvt = fHistCentrality->Integral(min,max);
  return nEvt;
}

//----------------------------------------------------------------------------
Double_t PlotUtilEmcalJetMass::GetNEventsAll(Int_t iList) const {

  TList *lst = static_cast<TList*>(fListArr.At(iList));
  if(!lst){
    Printf("Couldn't find list %d",iList);
    return 0x0;
  }

  TString histName = Form("fHistCentrality");
  TH1 *fHistCentrality = dynamic_cast<TH1*>(lst->FindObject(histName.Data()));
  Double_t nEvt = fHistCentrality->Integral();
  return nEvt;
}

//----------------------------------------------------------------------------
TH2D* PlotUtilEmcalJetMass::GetJetMassVsPt(JetSelection jSel, Int_t iList) {

  TString strJSel = "";
  if(jSel==kJetAll) strJSel = "AllSel";
  else if(jSel==kJetTagged) strJSel = "Tagged";
  else if(jSel==kJetTaggedMatch) strJSel = "TaggedMatch";
  else {
    Printf("Jet selection unknown. Returning null pointer.");
    return 0x0;
  }

  TList *lst = static_cast<TList*>(fListArr.At(iList));
  if(!lst){
    Printf("Couldn't find list %d",iList);
    return 0x0;
  }

  TString histName = Form("fh3PtJet1VsMassVsLeadPt%s_%d",strJSel.Data(),fCentBin);
  TH3 *fh3PtVsMassVsLeadPtJet1 = dynamic_cast<TH3*>(lst->FindObject(histName.Data()));
  if(!fh3PtVsMassVsLeadPtJet1) {
    Printf("Couldn't find fh3PtVsMassVsLeadPtJet1 %s. Returning null pointer.",histName.Data());
    return 0x0;
  }

  Int_t minz = fh3PtVsMassVsLeadPtJet1->GetZaxis()->FindBin(fMinLeadTrkPt+0.000001);
  Int_t maxz = fh3PtVsMassVsLeadPtJet1->GetZaxis()->FindBin(fMaxLeadTrkPt-0.000001);
  fh3PtVsMassVsLeadPtJet1->GetZaxis()->SetRange(minz,maxz);
  TH2D *hMPt = dynamic_cast<TH2D*>(fh3PtVsMassVsLeadPtJet1->Project3D("yx"));
  hMPt->SetName(Form("fh2MPt%s_Lst%d_Cent%d_LeadTrk%.0f_%.0f",strJSel.Data(),iList,fCentBin,fMinLeadTrkPt,fMaxLeadTrkPt));
  return hMPt;
}

//----------------------------------------------------------------------------
TGraphErrors* PlotUtilEmcalJetMass::GetMeanJetMassVsPt(JetSelection jSel, Int_t iList) {

  const Int_t nPtBins = 5;
  Double_t ptmin[nPtBins] = {20.,40.,60.,80.,100.};
  Double_t ptmax[nPtBins] = {40.,60.,80.,100.,120.};
  
  TGraphErrors *gr = new TGraphErrors();
  for(Int_t i = 0; i<nPtBins; i++) {
    SetJetPtRange(ptmin[i],ptmax[i]);
    TH1D *hM = GetJetMassDistribution(jSel,iList);
    gr->SetPoint(gr->GetN(),ptmin[i] + 0.5*(ptmax[i]-ptmin[i]), hM->GetMean());
    gr->SetPointError(gr->GetN()-1,0.5*(ptmax[i]-ptmin[i]), hM->GetMeanError());
  }
  return gr;
}

//----------------------------------------------------------------------------
TGraphErrors* PlotUtilEmcalJetMass::GetRMSJetMassVsPt(JetSelection jSel, Int_t iList) {

  const Int_t nPtBins = 5;
  Double_t ptmin[nPtBins] = {20.,40.,60.,80.,100.};
  Double_t ptmax[nPtBins] = {40.,60.,80.,100.,120.};
  
  TGraphErrors *gr = new TGraphErrors();
  for(Int_t i = 0; i<nPtBins; i++) {
    SetJetPtRange(ptmin[i],ptmax[i]);
    TH1D *hM = GetJetMassDistribution(jSel,iList);
    gr->SetPoint(gr->GetN(),ptmin[i] + 0.5*(ptmax[i]-ptmin[i]), hM->GetRMS());
    gr->SetPointError(gr->GetN()-1,0.5*(ptmax[i]-ptmin[i]), hM->GetRMSError());
  }
  return gr;
}

//----------------------------------------------------------------------------
TH3* PlotUtilEmcalJetMass::GetEPCorr3D(JetSelection jSel, Int_t iList) {

  TString strJSel = "";
  if(jSel==kJetAll) strJSel = "AllSel";
  else if(jSel==kJetTagged) strJSel = "Tagged";
  else if(jSel==kJetTaggedMatch) strJSel = "TaggedMatch";
  else {
    Printf("Jet selection unknown. Returning null pointer.");
    return 0x0;
  }

  TList *lst = static_cast<TList*>(fListArr.At(iList));
  if(!lst){
    Printf("Couldn't find list %d",iList);
    return 0x0;
  }

  TString histName = Form("fh3JetPtVsMassVsEPRel%s_%d",strJSel.Data(),fCentBin);
  TH3 *fh3JetPtVsMassVsEPRel = dynamic_cast<TH3*>(lst->FindObject(histName.Data()));
  if(!fh3JetPtVsMassVsEPRel) {
    Printf("Couldn't find fh3JetPtVsMassVsEPRel %s. Returning null pointer.",histName.Data());
    return 0x0;
  }
  return fh3JetPtVsMassVsEPRel;
}

//----------------------------------------------------------------------------
TH2D* PlotUtilEmcalJetMass::GetJetMassVsEP(JetSelection jSel, Int_t iList) {

  TH3 *fh3JetPtVsMassVsEPRel = GetEPCorr3D(jSel,iList);
  if(!fh3JetPtVsMassVsEPRel) return 0x0;

  Int_t minx = fh3JetPtVsMassVsEPRel->GetXaxis()->FindBin(fPtMin+0.00001);
  Int_t maxx = fh3JetPtVsMassVsEPRel->GetXaxis()->FindBin(fPtMax-0.00001);
  fh3JetPtVsMassVsEPRel->GetXaxis()->SetRange(minx,maxx);
  TH2D *h2 = dynamic_cast<TH2D*>(fh3JetPtVsMassVsEPRel->Project3D("yz"));
  return h2;
}

//----------------------------------------------------------------------------
TH2D* PlotUtilEmcalJetMass::GetJetPtVsEP(JetSelection jSel, Int_t iList) {

  TH3 *fh3JetPtVsMassVsEPRel = GetEPCorr3D(jSel,iList);
  if(!fh3JetPtVsMassVsEPRel) return 0x0;

  Int_t minx = fh3JetPtVsMassVsEPRel->GetXaxis()->FindBin(fPtMin+0.00001);
  Int_t maxx = fh3JetPtVsMassVsEPRel->GetXaxis()->FindBin(fPtMax-0.00001);
  fh3JetPtVsMassVsEPRel->GetXaxis()->SetRange(minx,maxx);
  TH2D *h2 = dynamic_cast<TH2D*>(fh3JetPtVsMassVsEPRel->Project3D("xz"));
  return h2;
}

//----------------------------------------------------------------------------
TH2D* PlotUtilEmcalJetMass::GetJetMassVsCent(JetSelection jSel, Int_t iList) {

  TString strJSel = "";
  if(jSel==kJetAll) strJSel = "AllSel";
  else if(jSel==kJetTagged) strJSel = "Tagged";
  else if(jSel==kJetTaggedMatch) strJSel = "TaggedMatch";
  else {
    Printf("Jet selection unknown. Returning null pointer.");
    return 0x0;
  }

  TList *lst = static_cast<TList*>(fListArr.At(iList));
  if(!lst){
    Printf("Couldn't find list %d",iList);
    return 0x0;
  }
  //fh3PtJet1VsMassVsCentAllSel,
  TString histName = Form("fh3PtJet1VsMassVsCent%s",strJSel.Data());
  TH3 *fh3JetPtVsMassVsCent = dynamic_cast<TH3*>(lst->FindObject(histName.Data()));
  if(!fh3JetPtVsMassVsCent) {
    Printf("Couldn't find fh3PtJet1VsMassVsCent %s. Returning null pointer.",histName.Data());
    //    lst->Print();
    return 0x0;
  }
  fh3JetPtVsMassVsCent->Sumw2();

  Int_t minx = fh3JetPtVsMassVsCent->GetXaxis()->FindBin(fPtMin+0.00001);
  Int_t maxx = fh3JetPtVsMassVsCent->GetXaxis()->FindBin(fPtMax-0.00001);
  fh3JetPtVsMassVsCent->GetXaxis()->SetRange(minx,maxx);
  TH2D *h2 = dynamic_cast<TH2D*>(fh3JetPtVsMassVsCent->Project3D("yz"));
  h2->SetName(Form("fh2MassVsCent_Lst%d",iList));
  return h2;
}

//----------------------------------------------------------------------------
TProfile* PlotUtilEmcalJetMass::GetJetMassVsEPProf(JetSelection jSel, Int_t iList) {

  TH2D *h2 = GetJetMassVsEP(jSel,iList);
  if(!h2) return 0x0;

  TProfile *p = dynamic_cast<TProfile*>(h2->ProfileX(Form("hpM%d",iList),1,-1));//,"s"));
  return p;
}

//----------------------------------------------------------------------------
TProfile* PlotUtilEmcalJetMass::GetJetPtVsEPProf(JetSelection jSel, Int_t iList) {

  TH2D *h2 = GetJetPtVsEP(jSel,iList);
  if(!h2) return 0x0;

  TProfile *p = dynamic_cast<TProfile*>(h2->ProfileX(Form("hpPt%d",iList),1,-1));//,"s"));
  return p;
}
