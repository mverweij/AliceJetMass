#ifndef __PlotUtilEmcalHJetMass_h__
#define __PlotUtilEmcalHJetMass_h__
#include "Rtypes.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TProfile.h"
#include "THnSparse.h"
#include "TFile.h"
#include "TList.h"
#include "THnSparse.h"
#include "TDirectoryFile.h"
#include "TRandom3.h"
#include "TLegend.h"
#include "TColor.h"

class TGraph;
class TGraphErrors;
class TArrayF;
class TArrayD;

//
// post processing AliAnalysisTaskEmcalHJetMass
//

class PlotUtilEmcalHJetMass {
 public:
  PlotUtilEmcalHJetMass();
  //PlotUtilEmcalHJetMass(const PlotUtilEmcalHJetMass& obj); // copy constructor
  //PlotUtilEmcalHJetMass& operator=(const PlotUtilEmcalHJetMass& other); // assignment
  virtual ~PlotUtilEmcalHJetMass() {;}

  enum JetSelection {
    kJetAll         = 0,
    kJetAllMatch    = 1,
    kJetTagged      = 2,
    kJetTaggedMatch = 3
  };

  void SetInputFileName(TString str = "AnalysisResults.root") { fFileInName = str; }
  void SetJetRadius(Double_t r)                               { fRadius     = r;   }
  void SetJetType(TString type = "Charged")                   { fJetType    = type;}
  void SetTag(TString tag)                                    { fTag        = tag; }
  void SetConstTag(TString tag = "ConstSub")                  { fConstTag   = tag; }
  void SetCentBin(Int_t cb = 0)                               { fCentBin    = cb;  }

  void SetJetPtRange(Double_t min, Double_t max)              { fPtMin = min; fPtMax = max; }
  void SetHadronPtRange(Double_t min, Double_t max)           { fPtMinH = min; fPtMaxH = max; }

  Bool_t LoadFile();
  Bool_t LoadList();

  void CreateJetRecoilDistributions(JetSelection jSel, Int_t iList);

  TH1 *GetJetRecoilPt() const                 { return fhPtJetRecoil; }
  TH1 *GetJetRecoilM()  const                 { return fhMJetRecoil ; }
  TH1 *GetDifference(TH1 *h1, TH1 *h2);

 private:

 protected:
  TString          fFileInName;
  TFile           *fFileIn;  //input file for signal
  TObjArray        fListArr;
  Double_t         fRadius;
  Double_t         fPtMin;
  Double_t         fPtMax;
  Double_t         fPtMinH;
  Double_t         fPtMaxH;
  TString          fJetType;
  TString          fTag;
  TString          fConstTag;
  Int_t            fCentBin;
  TH1             *fhPtJetRecoil;
  TH1             *fhMJetRecoil;
};
#endif
