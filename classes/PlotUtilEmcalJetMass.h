#ifndef __PlotUtilEmcalJetMass_h__
#define __PlotUtilEmcalJetMass_h__
#include "Rtypes.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TProfile.h"
#include "THnSparse.h"
#include "TFile.h"
#include "TList.h"
#include "THnSparse.h"
#include "TDirectoryFile.h"
#include "TRandom3.h"
#include "TLegend.h"
#include "TColor.h"

class TGraph;
class TGraphErrors;
class TArrayF;
class TArrayD;

//
// post processing AliAnalysisTaskEmcalJetMass
//

class PlotUtilEmcalJetMass {
 public:
  PlotUtilEmcalJetMass();
  //PlotUtilEmcalJetMass(const PlotUtilEmcalJetMass& obj); // copy constructor
  //PlotUtilEmcalJetMass& operator=(const PlotUtilEmcalJetMass& other); // assignment
  virtual ~PlotUtilEmcalJetMass() {;}

  enum JetSelection {
    kJetAll         = 0,
    kJetAllMatch    = 1,
    kJetTagged      = 2,
    kJetTaggedMatch = 3
  };

  void SetInputFileName(TString str = "AnalysisResults.root") { fFileInName = str; }
  void SetJetRadius(Double_t r)                               { fRadius     = r;   }
  void SetJetType(TString type = "Charged")                   { fJetType    = type;}
  void SetSuffix(TString suf)                                 { fSuffix     = suf; }
  void SetTag(TString tag)                                    { fTag        = tag; }
  void SetConstTag(TString tag = "ConstSub")                  { fConstTag   = tag; }
  void SetCentBin(Int_t cb = 0)                               { fCentBin    = cb;  }

  void SetJetPtRange(Double_t min, Double_t max)              { fPtMin = min; fPtMax = max; }
  void SetMinLeadTrackPt(Double_t min)                        { fMinLeadTrkPt = min; }
  void SetMaxLeadTrackPt(Double_t max)                        { fMaxLeadTrkPt = max; }

  Bool_t LoadFile();
  Bool_t LoadList();

  TH1     *GetCentralityHist(Int_t iList) const;
  Double_t GetNEvents(Int_t iList) const;
  Double_t GetNEventsAll(Int_t iList) const;

  TH1D *GetJetMassDistribution(JetSelection jSel, Int_t iList);
  TH1D *GetJetMassERatioDistribution(JetSelection jSel, Int_t iList);
  TH1D *GetJetPtDistribution(JetSelection jSel, Int_t iList);

  TH2D *GetJetMassVsPt(JetSelection jSel, Int_t iList);
  TH2D *GetJetMassVsEP(JetSelection jSel, Int_t iList);
  TH2D *GetJetPtVsEP(JetSelection jSel, Int_t iList);
  TH2D *GetJetMassVsCent(JetSelection jSel, Int_t iList);

  TGraphErrors *GetMeanJetMassVsPt(JetSelection jSel, Int_t iList);
  TGraphErrors *GetRMSJetMassVsPt(JetSelection jSel, Int_t iList);

  TProfile *GetJetMassVsEPProf(JetSelection jSel, Int_t iList);
  TProfile *GetJetPtVsEPProf(JetSelection jSel, Int_t iList);

  TH3 *GetEPCorr3D(JetSelection jSel, Int_t iList);

 private:

 protected:
  TString          fFileInName;
  TFile           *fFileIn; //input file
  TObjArray        fListArr;
  Double_t         fRadius;
  Double_t         fPtMin;
  Double_t         fPtMax;
  TString          fSuffix;
  TString          fJetType;
  TString          fTag;
  TString          fConstTag;
  Int_t            fCentBin;
  Double_t         fMinLeadTrkPt;
  Double_t         fMaxLeadTrkPt;
};
#endif
