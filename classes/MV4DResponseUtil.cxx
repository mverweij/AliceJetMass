#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMath.h"
#include "Riostream.h"
#include "TH1.h"
#include "TRandom.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "THnSparse.h"
#include "TArrayF.h"
#include "TArrayD.h"
#include <TRandom3.h>
#include "TLegend.h"
#include "TLatex.h"
#include "TLine.h"
#include "TVirtualFitter.h"
#include "TList.h"

#include "MV4DResponseUtil.h"

using std::cout;
using std::endl;

ClassImp(MV4DResponseUtil)

//-------------------------------------------------------------------------
MV4DResponseUtil::MV4DResponseUtil():
  fResponse(0x0),
  fResponseReduced(0x0),
  fRespFunc(0x0),
  fRespHist(0x0),
  fFuncPrior(0x0),
  fHistPrior(0x0),
  fiMSub(0),
  fiMTru(1),
  fiPtSub(2),
  fiPtTru(3)
{
  //constructor
}

//-------------------------------------------------------------------------
Bool_t MV4DResponseUtil::Init() {
  
  Bool_t prep = PrepareReducedResponse();
  if(!prep) return kFALSE;

  if(fFuncPrior) WeightWithPriorFunc();
  if(fHistPrior) WeightWithPriorHist();

  return kTRUE;
}

//-------------------------------------------------------------------------
Bool_t MV4DResponseUtil::WeightWithPriorFunc() {

  if(!fFuncPrior) return kFALSE;

  if(fRespFunc) delete fRespFunc;
  fRespFunc = dynamic_cast<THnSparse*>(fResponseReduced->Clone("fRespFunc"));
  fRespFunc->Reset();

  const Int_t ndimPtTrue = fResponseReduced->GetAxis(fiPtTru)->GetNbins();
  Double_t weight[ndimPtTrue];// = {0};
  TH1 *hPtTrue = dynamic_cast<TH1*>(fResponseReduced->Projection(fiPtTru));
  for(Int_t i = 1; i<ndimPtTrue; i++) {
    Double_t integ = fFuncPrior->Integral(hPtTrue->GetXaxis()->GetBinLowEdge(i),hPtTrue->GetXaxis()->GetBinUpEdge(i));
    Double_t width = hPtTrue->GetXaxis()->GetBinWidth(i);
    if(width>0.) integ/=width;
    weight[i] = integ/hPtTrue->GetBinContent(i);
  }

  Int_t nDim = fResponseReduced->GetNdimensions();
  Int_t* coord = new Int_t[nDim];
  for(Int_t i =1; i<fResponseReduced->GetNbins(); i++) {
    Double_t w = fResponseReduced->GetBinContent(i,coord);
    Double_t scale = weight[coord[fiPtTru]];
    fRespFunc->SetBinContent(coord,w*scale);
  }
  delete [] coord;

  return kTRUE;
}

//-------------------------------------------------------------------------
Bool_t MV4DResponseUtil::WeightWithPriorHist() {

  if(!fHistPrior) return kFALSE;

  if(fRespHist) delete fRespHist;
  fRespHist = dynamic_cast<THnSparse*>(fResponseReduced->Clone("fRespHist"));
  fRespHist->Reset();

  const Int_t ndimPtTrue = fResponseReduced->GetAxis(fiPtTru)->GetNbins();
  Double_t weight[ndimPtTrue];
  TH1 *hPtTrue = dynamic_cast<TH1*>(fResponseReduced->Projection(fiPtTru));
  for(Int_t i = 1; i<ndimPtTrue; i++) {
    Double_t min = hPtTrue->GetXaxis()->GetBinLowEdge(i);
    Double_t max = hPtTrue->GetXaxis()->GetBinUpEdge(i);
    //find bin edges in fHistPrior
    Int_t bmin = fHistPrior->GetXaxis()->FindBin(min+0.000001);
    Int_t bmax = fHistPrior->GetXaxis()->FindBin(max-0.000001);
    Double_t integ = fHistPrior->Integral(bmin,bmax,"width");
    Double_t width = hPtTrue->GetXaxis()->GetBinWidth(i);
    if(width>0.) integ/=width;
    weight[i] = integ/hPtTrue->GetBinContent(i);
  }

  Int_t nDim = fResponseReduced->GetNdimensions();
  Int_t* coord = new Int_t[nDim];
  for(Int_t i =1; i<fResponseReduced->GetNbins(); i++) {
    Double_t w = fResponseReduced->GetBinContent(i,coord);
    Double_t scale = weight[coord[fiPtTru]];
    fRespHist->SetBinContent(coord,w*scale);
  }
  delete [] coord;

  return kTRUE;
}

//-------------------------------------------------------------------------
Bool_t MV4DResponseUtil::PrepareReducedResponse() {

  //Will just work with 4 axises: MSub, MTrue, PtSub, PtTrue
  //In case fResponse has more reduce to the first for

  if(fResponseReduced) return kTRUE;
  if(!fResponse) {
    Printf("fResponse unknown. Use SetResponse(THnSparse *hn)");
    return kFALSE;
  }

  Int_t ndim = fResponse->GetNdimensions();
  if(ndim<4) {
    Printf("fResponse has too little dimensions: %d",ndim);
    return kFALSE;
  } else if(ndim==4) {
    fResponseReduced = fResponse;
    return kTRUE;
  } else if(ndim>4) {
    //reduce matrix
    const Int_t ndimWant = 4;
    Int_t dim[ndimWant] = {fiMSub,fiMTru,fiPtSub,fiPtTru};
    fResponseReduced = static_cast<THnSparse*>(fResponse->ProjectionND(ndimWant,dim,"E"));
    return kTRUE;
  }

  Printf("Should not end up here");
  return kFALSE;
}


