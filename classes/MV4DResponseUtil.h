#ifndef __MV4DResponseUtil_h__
#define __MV4DResponseUtil_h__
#include "Rtypes.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TProfile.h"
#include "THnSparse.h"
#include "TFile.h"
#include "TList.h"
#include "THnSparse.h"
#include "TDirectoryFile.h"
#include "TRandom3.h"
#include "TLegend.h"
#include "TColor.h"

class TGraph;
class TGraphErrors;
class TArrayF;
class TArrayD;

//
// Utilities for 4D response matrices
//

class MV4DResponseUtil {
 public:
  MV4DResponseUtil();
  //MV4DResponseUtil(const MV4DResponseUtil& obj); // copy constructor
  //MV4DResponseUtil& operator=(const MV4DResponseUtil& other); // assignment
  virtual ~MV4DResponseUtil() {;}

  THnSparse         *GetResponse()  const          { return fResponse  ; }
  THnSparse         *GetResponseReduced()  const   { return fResponseReduced  ; }
  THnSparse         *GetRespFunc()  const          { return fRespFunc  ; }
  THnSparse         *GetRespHist()  const          { return fRespHist  ; }
  TF1               *GetPriorFunc() const          { return fFuncPrior ; }
  TH1               *GetPriorHist() const          { return fHistPrior ; }

  void               SetResponse(THnSparse *hn)    { fResponse    = hn ; }
  void               SetPrior(TF1 *f1)             { fFuncPrior   = f1 ; }
  void               SetPrior(TH1 *f1)             { fHistPrior   = f1 ; }

  void               SetAxisesResponse(Int_t iMSub, Int_t iMTru, Int_t iPtSub, Int_t iPtTru) {fiMSub=iMSub; fiMTru=iMTru; fiPtSub=iPtSub; fiPtTru=iPtTru;}

  Bool_t             Init();

 protected:
  Bool_t             PrepareReducedResponse();
  Bool_t             WeightWithPriorFunc();
  Bool_t             WeightWithPriorHist();

 private:
  THnSparse         *fResponse;        //multi-dimensional response
  THnSparse         *fResponseReduced; //multi-dimensional response
  THnSparse         *fRespFunc;        //multi-dim resp weighted with fFuncPrior
  THnSparse         *fRespHist;        //multi-dim resp weighted with fHistPrior
  TF1               *fFuncPrior;       //TF1 prior
  TH1               *fHistPrior;       //TH1 prior
  Int_t              fiMSub;           //Msub axis of response
  Int_t              fiMTru;           //Mtrue axis of response
  Int_t              fiPtSub;          //PtSub axis of response
  Int_t              fiPtTru;          //PtTrue axis of response
};
#endif
