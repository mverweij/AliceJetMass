#include "PlotUtilEmcalHJetMass.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMath.h"
#include "Riostream.h"
#include "TH1.h"
#include "TRandom.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "THnSparse.h"
#include "TArrayF.h"
#include "TArrayD.h"
#include <TRandom3.h>
#include "TLegend.h"
#include "TLatex.h"
#include "TLine.h"
#include "TVirtualFitter.h"
#include "TList.h"

#define round(x) ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))

using std::cout;
using std::endl;

ClassImp(PlotUtilEmcalHJetMass)

//----------------------------------------------------------------------------
PlotUtilEmcalHJetMass::PlotUtilEmcalHJetMass():
  fFileInName(0),
  fFileIn(0),
  fListArr(0),
  fRadius(0.4),
  fPtMin(-1e6),
  fPtMax(1e6),
  fPtMinH(20.),
  fPtMaxH(50.),
  fJetType("Charged"),
  fTag(""),
  fConstTag(""),
  fCentBin(0)
{
  fListArr.SetOwner(kTRUE);
}

//----------------------------------------------------------------------------
Bool_t PlotUtilEmcalHJetMass::LoadFile() {

  if(!fFileInName.IsNull())
    fFileIn = new TFile(fFileInName.Data());
  else {
    Printf("No file name provided: %s",fFileInName.Data());
    return kFALSE;
  }

  //fFileIn->ls();
  return kTRUE;
}

//----------------------------------------------------------------------------
Bool_t PlotUtilEmcalHJetMass::LoadList() {

  if(!fFileIn) {
    Printf("Cannot load list without file.");
    return kFALSE;
  }

  TString strTrk = "PicoTracks_pT0150";
  if(fJetType.Contains("MCParticlesSelected")) strTrk = "MCParticlesSelected_pT0000";
  TString strClus = "";
  if(fJetType.Contains("Full")) strClus = "CaloClustersCorr";
  TString strJet = "Jet";
  if(fJetType.Contains("JetEmbOnly")) strJet = "JetEmbOnly";
  TString strJetType = "Charged";
  if(fJetType.Contains("Full")) strJetType = "Full";
  TString listName = Form("HJetMass_%s_AKT%sR%03d_%s_%sE_scheme%s_TC%s",strJet.Data(),strJetType.Data(),(Int_t)(fRadius*100),strTrk.Data(),strClus.Data(),fConstTag.Data(),fTag.Data());
  TList *lst = dynamic_cast<TList*>(fFileIn->Get(listName.Data()));
  if(!lst) {
    Printf("Couldn't find list %s",listName.Data());
    fListArr.Add(0x0);
    return kFALSE;
  }
  fListArr.Add(lst);
  return kTRUE;
}

//----------------------------------------------------------------------------
void PlotUtilEmcalHJetMass::CreateJetRecoilDistributions(JetSelection jSel, Int_t iList) {

  TString strJSel = "";
  if(jSel==kJetAll) strJSel = "AllSel";
  else if(jSel==kJetAllMatch) strJSel = "AllSelMatch";
  else if(jSel==kJetTagged) strJSel = "Tagged";
  else if(jSel==kJetTaggedMatch) strJSel = "TaggedMatch";
  else {
    Printf("Jet selection unknown. Returning null pointer.");
    return;
  }

  TList *lst = static_cast<TList*>(fListArr.At(iList));
  if(!lst){
    Printf("Couldn't find list %d",iList);
    return;
  }

  TString strPtHadron = Form("fh1PtHadron_%d",fCentBin);
  if(jSel==kJetAllMatch || jSel==kJetTaggedMatch) strPtHadron = Form("fh1PtHadronMatch_%d",fCentBin);
  TH1 *fh1PtHadron = dynamic_cast<TH1*>(lst->FindObject(strPtHadron.Data()));
  Int_t minpth = fh1PtHadron->GetXaxis()->FindBin(fPtMinH+0.00001);
  Int_t maxpth = fh1PtHadron->GetXaxis()->FindBin(fPtMaxH-0.00001);
  Double_t ntrig = fh1PtHadron->Integral(minpth,maxpth);

  TString histName = Form("fh3PtJet1VsMassVsHPt%s_%d",strJSel.Data(),fCentBin);
  TH3 *fh3PtJetVsMassVsHPt = dynamic_cast<TH3*>(lst->FindObject(histName.Data()));
  if(!fh3PtJetVsMassVsHPt){
    Printf("Couldn't find fh3PtJetVsMassVsHPt %s",histName.Data());
    return;
  }
  fh3PtJetVsMassVsHPt->Sumw2();

  Int_t minx = fh3PtJetVsMassVsHPt->GetXaxis()->FindBin(fPtMin+0.00001);
  Int_t maxx = fh3PtJetVsMassVsHPt->GetXaxis()->FindBin(fPtMax-0.00001);
  Int_t minz = fh3PtJetVsMassVsHPt->GetZaxis()->FindBin(fPtMinH+0.00001);
  Int_t maxz = fh3PtJetVsMassVsHPt->GetZaxis()->FindBin(fPtMaxH-0.00001);

  fhMJetRecoil = dynamic_cast<TH1D*>(fh3PtJetVsMassVsHPt->ProjectionY(Form("fhMJetRecoil%s_Lst%d_Cent%d_TT%.0f%.0f",strJSel.Data(),iList,fCentBin,fPtMinH,fPtMaxH),minx,maxx,minz,maxz));
  fhPtJetRecoil = dynamic_cast<TH1D*>(fh3PtJetVsMassVsHPt->ProjectionX(Form("fhPtJetRecoil%s_Lst%d_Cent%d_TT%.0f%.0f",strJSel.Data(),iList,fCentBin,fPtMinH,fPtMaxH),1,-1,minz,maxz));

  if(ntrig>0.) {
    fhMJetRecoil->Scale(1./ntrig);
    fhPtJetRecoil->Scale(1./ntrig);
  } else
    Printf("Warning: number of triggers has unexpected value: %f",ntrig);
}

//----------------------------------------------------------------------------
TH1* PlotUtilEmcalHJetMass::GetDifference(TH1 *h1, TH1 *h2) {
  //get difference between distributions
  TH1 *hd = (TH1*)h1->Clone("hd");
  hd->Reset();

  //check if axis of two histos the same
  if(h1->GetNbinsX() != h2->GetNbinsX()) {
    printf("bins of histos is not the same (%d vs %d). Aborting.", h1->GetNbinsX(), h2->GetNbinsX());
    return 0;
  }
  if(h1->GetXaxis()->GetXmax() != h2->GetXaxis()->GetXmax()) {
    printf("max of x-axis of histos is not the same (%f vs %f). Aborting.", h1->GetXaxis()->GetXmax(), h2->GetXaxis()->GetXmax());
    return 0;
  }

  for(Int_t i = 1; i<=h1->GetNbinsX(); i++) {
    Double_t diff = h1->GetBinContent(i) - h2->GetBinContent(i);
    hd->SetBinContent(i,diff);
    Double_t err2 = h1->GetBinError(i)*h1->GetBinError(i) - h2->GetBinError(i)*h2->GetBinError(i);
    if(err2>0.)
      hd->SetBinError(i,TMath::Sqrt(err2));
  }

  return hd;
}
