#include "PtHardBinUtilities.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMath.h"
#include "Riostream.h"
#include "TH1.h"
#include "TRandom.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TF1.h"
#include "THnSparse.h"
#include "TArrayF.h"
#include "TArrayD.h"

#define round(x) ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))

using std::cout;
using std::endl;

ClassImp(PtHardBinUtilities)

PtHardBinUtilities::PtHardBinUtilities():
  fUseDefaultWeights13b4(kFALSE),
  fUseDefaultWeights12a15e(kFALSE),
  fFileIn(0x0),
  fListFiles(0x0),
  fArrWeights(0x0),
  fArrXsec(0x0),
  fArrTrials(0x0),
  fNameWeightList("DiJetR040MCParticlespT0000RhoType0"),
  fNameHistosDir("DiJetR040MCParticlespT0000RhoType0"),
  fNameHistosList("DiJetR040MCParticlespT0000RhoType0"),
  fListHistos(0x0),
  fObjArrayHistoNames(0x0),
  fOutput(0x0),
  fOutDir(0x0),
  fMinStat(0),
  fMinStatHn(0)
{;}

//----------------------------------------------------------------------------
PtHardBinUtilities::PtHardBinUtilities(const PtHardBinUtilities& obj):
  fUseDefaultWeights13b4(obj.fUseDefaultWeights13b4),
  fUseDefaultWeights12a15e(obj.fUseDefaultWeights12a15e),
  fFileIn(obj.fFileIn),
  fListFiles(obj.fListFiles),
  fArrWeights(obj.fArrWeights),
  fArrXsec(obj.fArrXsec),
  fArrTrials(obj.fArrTrials),
  fNameWeightList(obj.fNameWeightList),
  fNameHistosDir(obj.fNameHistosDir),
  fNameHistosList(obj.fNameHistosList),
  fListHistos(obj.fListHistos),
  fObjArrayHistoNames(obj.fObjArrayHistoNames),
  fOutput(obj.fOutput),
  fOutDir(obj.fOutDir),
  fMinStat(obj.fMinStat),
  fMinStatHn(obj.fMinStatHn)
{;}

//----------------------------------------------------------------------------
void PtHardBinUtilities::CleanMemory() { 

  if(!fListHistos || fListHistos->GetEntries()<1) return;

  TList *histSpec2;
  for(int ifile=0; ifile<fListFiles->GetEntries(); ifile++) {
    histSpec2 = (TList*)fListHistos->At(ifile);
    if(histSpec2) histSpec2->Clear();
  }

  if(fListHistos && fListHistos->GetEntries()>0) fListHistos->Clear();
}

//----------------------------------------------------------------------------
Bool_t PtHardBinUtilities::InitFileList(TString list) {

  if(fListFiles) {
    fListFiles->Clear();
    delete fListFiles;
  }
  fListFiles = new TList();
  Printf("Reading %s", list.Data());
  ifstream inputFile;
  inputFile.open(list.Data());
  TString line;

  while( inputFile>>line) {
     Printf("%s", line.Data());
    TFile * fMC = TFile::Open(line.Data());
    if(!fMC) return kFALSE;
    fListFiles->Add(fMC);
  }

  return kTRUE;

}

//----------------------------------------------------------------------------
void PtHardBinUtilities::InitWeights() {
  //
  // Store weights in TArrayF
  //

  fArrWeights = new TArrayD(fListFiles->GetEntries());
  fArrXsec    = new TArrayF(fListFiles->GetEntries());
  fArrTrials  = new TArrayF(fListFiles->GetEntries());
  
  //default weights are for LHC13b4 simulation (boosted PYTHIA Perugia2011 5.02 TeV anchored to LHC13b/c/d/e)
  Double_t defaultTrials[10] = {8.961553e+06,4.296729e+06,3.992884e+06,3.863750e+06,4.641894e+06,4.469303e+06,4.232103e+06,4.207111e+06,4.193115e+06,1.642000e+03};
  Double_t defaultXsec[10] = {2.657047e+01,1.602235e+00,1.279304e-01,1.346681e-02,1.748998e-03,2.832155e-04,5.270665e-05,1.342506e-05,3.868738e-06,3.947380e-06};
  //default weights are for LHC12a15e simulation (2.76 TeV; anchored to Pb-Pb LHC11h)
  //from lego train  
  //Double_t kPtHardBinsScaling[10] = {5.206101E-05, 5.859497E-06, 4.444755E-07, 4.344664E-08, 5.154750E-09, 6.956634E-10, 1.149828E-10, 2.520137E-11, 6.222240E-12, 2.255832E-12};
  //from MV
  Double_t kPtHardBinsScaling[10] = {9.299330e-06,1.177064e-06,1.088605e-07,8.200242e-09,1.322483e-09,1.478901e-10,2.469288e-11,5.228472e-12,1.396930e-12,5.044158e-13};
  TList *histServ = 0x0; 
  Double_t defaultWeight[10];
  for(int ifile=0; ifile<fListFiles->GetEntries(); ifile++) {
   
    if(fUseDefaultWeights13b4) {
      defaultWeight[ifile] = defaultXsec[ifile]/defaultTrials[ifile];
      Printf("defaults weight 13b4: %e",defaultWeight[ifile]);
      fArrWeights->SetAt(defaultWeight[ifile],ifile);
      fArrXsec->SetAt(defaultXsec[ifile],ifile);
      fArrTrials->SetAt(defaultTrials[ifile],ifile);
    } else if(fUseDefaultWeights12a15e) {
      defaultWeight[ifile] = kPtHardBinsScaling[ifile];
      fArrWeights->SetAt(defaultWeight[ifile],ifile);
      Printf("defaults weight 12a15e: %e",defaultWeight[ifile]);
    } else {

      TFile *f = (TFile*)fListFiles->At(ifile); 
      histServ = (TList*)f->Get(fNameWeightList.Data());
      if(!histServ) {
	Printf("fNameWeightList does not exist %s.",fNameWeightList.Data());
	continue;
      }
      Printf("Weighting list found: %s. Content: ",fNameWeightList.Data());
      //      histServ->Print();

      TProfile *fh1Xsec = (TProfile*)histServ->FindObject("fHistXsection");
      TH1F *fh1Trials = (TH1F*)histServ->FindObject("fHistTrials");//SelEvents");
      if(!fh1Trials) {
        Printf("WARNING: couldn't find fHistTrials. Trying now fHistTrialsSelEvents");
        fh1Trials = (TH1F*)histServ->FindObject("fHistTrialsSelEvents");
      }
      if(!fh1Xsec)
        Printf("WARNING: couldn't find fHistXsection. Falling back to default weights");

      if(!fh1Xsec || !fh1Trials) {
	defaultWeight[ifile] = defaultXsec[ifile]/defaultTrials[ifile];
	Printf("defaults weight: %e",defaultWeight[ifile]);
	fArrWeights->SetAt(defaultWeight[ifile],ifile);
	fArrXsec->SetAt(defaultXsec[ifile],ifile);
	fArrTrials->SetAt(defaultTrials[ifile],ifile);
	histServ->Clear();
	continue;
      }

      Int_t pthardBin = 0;
      for(int i =1; i<=fh1Xsec->GetNbinsX(); i++) {
	if(fh1Xsec->GetBinContent(i)>0) {
	  pthardBin = i;
          Printf("pthardBin: %d",pthardBin);
	  Printf("%d %d fh1Xsec->GetBinContent(i) = %f",ifile,i,fh1Xsec->GetBinContent(i));
	}
      }

      Float_t weight = 0.;
      if(fh1Trials->GetBinContent(pthardBin)>0.)
	weight = fh1Xsec->GetBinContent(pthardBin)/fh1Trials->GetBinContent(pthardBin);

      fArrWeights->SetAt(weight,ifile);
      fArrXsec->SetAt(fh1Xsec->GetBinContent(pthardBin),ifile);
      fArrTrials->SetAt(fh1Trials->GetBinContent(pthardBin),ifile);

      Printf("ifile %d xsec = %e trials = %e weight = %e",ifile,fh1Xsec->GetBinContent(pthardBin),fh1Trials->GetBinContent(pthardBin),weight);

      histServ->Clear();
    }
  }
  Printf("InitWeights done");

  // printf("defaultTrials[10] = {");
  // for(int ifile=0; ifile<fListFiles->GetEntries(); ifile++)
  //   printf("%e,",defaultTrials[ifile]);
  // printf("\n");
  // printf("defaultXsec[10] = {");
  // for(int ifile=0; ifile<fListFiles->GetEntries(); ifile++)
  //   printf("%e,",defaultXsec[ifile]);
  // printf("\n");
}

//----------------------------------------------------------------------------
Bool_t PtHardBinUtilities::CreateOutputList() {

  //  if(fOutput) fOutput->Clear();

  TList *list = dynamic_cast<TList*>(fListHistos->At(0));
  if(!list) {
    Printf("Error: need list to clone for ouput file");
    return kFALSE;
  }

  //  fOutDir = new TDirectoryFile(fNameHistosDir.Data(),fNameHistosDir.Data());
  //  fOutDir = new TDirectoryFile(fNameHistosDir.Data(),Form("%s/%sMerged",fNameHistosDir.Data(),list->GetName()));
  fOutput = static_cast<TList*>(list->Clone(Form("%s",list->GetName())));
  Printf("fOutput created!!! ");
  fOutput->ls();
  //  fOutDir->Add(fOutput);

  //fOutput->SetOwner(kTRUE);
  // fOutput = new TList();
  // fOutDir->WriteTObject(fOutput,fOutput->GetName());

  
  TH1 *h1 = 0x0;
  TH2 *h2 = 0x0;
  TH3 *h3 = 0x0;
  THnSparse *hn = 0x0; 

  for (Int_t i=0; i<fOutput->GetEntries(); ++i) {
    if(fOutput->At(i)->InheritsFrom("THnSparse")) {
      hn = dynamic_cast<THnSparse*>(fOutput->At(i));
      if(hn) {
        hn->Sumw2(); 
        hn->Reset();
        continue;
      }
    }
    else if(fOutput->At(i)->InheritsFrom("TH3")) {
      h3 = dynamic_cast<TH3*>(fOutput->At(i));
      if(h3) {
        h3->Sumw2(); 
        h3->Reset();
        continue;
      }
    }
    else if(fOutput->At(i)->InheritsFrom("TH1")) {
      h2 = dynamic_cast<TH2*>(fOutput->At(i));
      if(h2) {
        h2->Sumw2();
        h2->Reset();
        continue;
      }
    }
    else if(fOutput->At(i)->InheritsFrom("TH1")) {
      h1 = dynamic_cast<TH1*>(fOutput->At(i));
      if(h1) { 
        h1->Sumw2();
        h1->Reset();
        continue;
      }
    }
  }

  Printf("Print fOutDir");
  //  fOutput->Print();

  //  fOutput->ls();
  return kTRUE;
}

//----------------------------------------------------------------------------
Bool_t PtHardBinUtilities::CreateOutputListDiJetAna() {

  //  if(fOutput) fOutput->Clear();
  TList *list = dynamic_cast<TList*>(fListHistos->At(0));
  if(!list) {
    Printf("Error: need list to clone for ouput file");
    return kFALSE;
  }

  //  fOutDir = new TDirectoryFile(fNameHistosDir.Data(),fNameHistosDir.Data());
  //  fOutDir = new TDirectoryFile(fNameHistosDir.Data(),Form("%s/%sMerged",fNameHistosDir.Data(),list->GetName()));
  fOutput = static_cast<TList*>(list->Clone(Form("%s",list->GetName())));
  Printf("fOutput ANA created!!! ");
  fOutput->ls();
  //  fOutDir->Add(fOutput);

  //fOutput->SetOwner(kTRUE);
  // fOutput = new TList();
  // fOutDir->WriteTObject(fOutput,fOutput->GetName());

  
  TH1 *h1 = 0x0;
  TH2 *h2 = 0x0;
  TH3 *h3 = 0x0;
  THnSparse *hn = 0x0; 

  for (Int_t i=0; i<fOutput->GetEntries(); ++i) {

    h1 = dynamic_cast<TH1*>(fOutput->At(i));
    if(h1) { 
      //fOutput->RemoveAt(i);
      h1->Reset();
      continue;
    }
    h2 = dynamic_cast<TH2*>(fOutput->At(i));
    if(h2) {
      //fOutput->RemoveAt(i);
      h2->Reset();
      continue;
    }
    h3 = dynamic_cast<TH3*>(fOutput->At(i));
    if(h3) { 
      //fOutput->RemoveAt(i);
      h3->Reset();
      continue;
    }
    hn = dynamic_cast<THnSparse*>(fOutput->At(i));
    if(hn) { 
      //TString name(hn->GetName());
      //if(name.EqualTo("fhnDiJetVarsCh"))
      hn->Reset();
      //else
      //fOutput->RemoveAt(i);
      continue;
    }
  }

  Printf("Print fOutDir");
  //  fOutput->Print();

  //  fOutput->ls();
  return kTRUE;
}


//----------------------------------------------------------------------------
Bool_t PtHardBinUtilities::SetObjArrayHistoNames() {

  TList *list = (TList*)fListHistos->At(0);
  if(!list) {
    Printf("no list found");
    return kFALSE;
  }

  if(!fObjArrayHistoNames) {
    fObjArrayHistoNames = new TObjArray();
    fObjArrayHistoNames->SetOwner(kTRUE);
  }

  fObjArrayHistoNames->Clear();
  
  TH1 *h1 = 0x0;
  TH2 *h2 = 0x0;
  TH3 *h3 = 0x0;
  THnSparse *hn = 0x0; 

  for (Int_t i=0; i<list->GetEntries(); ++i) {

    TObjString* ostr = NULL;

    h1 = dynamic_cast<TH1*>(list->At(i));
    if(h1) 
      ostr = new TObjString(h1->GetName());

    h2 = dynamic_cast<TH2*>(list->At(i));
    if(h2) 
      ostr = new TObjString(h2->GetName());
    
    h3 = dynamic_cast<TH3*>(list->At(i));
    if(h3) 
      ostr = new TObjString(h3->GetName());

    hn = dynamic_cast<THnSparse*>(list->At(i));
    if(hn) 
      ostr = new TObjString(hn->GetName());

    fObjArrayHistoNames->Add(ostr);
  }

  //  fObjArrayHistoNames->SetName("fObjArrayHistoNames");
  
  return kTRUE;
}

//----------------------------------------------------------------------------
void PtHardBinUtilities::DoWeighting() {

  Printf(">>PtHardBinUtilities::DoWeighting()");

  if(fListFiles->GetEntries()<1) {
     Printf("No files in list");
     return;
  }
  if(!fArrWeights) InitWeights();

  if(!InitHistosList()) {
    Printf("InitHistosList failed");
    return;
  }

  if(!SetObjArrayHistoNames()) {
    Printf("SetObjArrayHistoNames failed");
    return;//
  }

  if(!CreateOutputListDiJetAna()) {
    Printf("CreateOutputListDiJetAna failed");
    return;
  }

  TList *list = 0x0;
  TH1 *h1 = 0x0;
  TH2 *h2 = 0x0;
  TH3 *h3 = 0x0;
  THnSparse *hn = 0x0; 
  TH1 *h1tmp = 0x0;
  TH2 *h2tmp = 0x0;
  TH3 *h3tmp = 0x0;
  THnSparse *hntmp = 0x0; 
  for(Int_t ifile=0; ifile<fListFiles->GetEntries(); ifile++) {
    list = (TList*)fListHistos->At(ifile);
    //list->Print();

    for (Int_t i=0; i<fOutput->GetEntries(); ++i) {

      Int_t bAdded = 0;
      if(fOutput->At(i)->InheritsFrom("THnSparse")) {
        hn = dynamic_cast<THnSparse*>(fOutput->At(i));
	hntmp = static_cast<THnSparse*>(list->At(i));
        if(fMinStat>0) RemoveLowStatBinsTHnSparse(hntmp);
        //  hntmp->Sumw2();
	hntmp->Scale(fArrWeights->At(ifile));
	hn->Add(hntmp);
        Printf("scaled %s",hntmp->GetName());
        bAdded++;
      }
      else if(fOutput->At(i)->InheritsFrom("TH3")) {
        h3 = dynamic_cast<TH3*>(fOutput->At(i));
      	h3tmp = static_cast<TH3*>(list->At(i));
        h3tmp->Sumw2();
        if(fMinStat>0) RemoveLowStatBinsTH3(h3tmp);
      	h3tmp->Scale(fArrWeights->At(ifile));
      	h3->Add(h3tmp);
        bAdded++;
      }
      else if(fOutput->At(i)->InheritsFrom("TH2")) {
        h2 = dynamic_cast<TH2*>(fOutput->At(i));
      	h2tmp = static_cast<TH2*>(list->At(i));
        h2tmp->Sumw2();
        if(fMinStat>0) RemoveLowStatBinsTH2(h2tmp);
      	h2tmp->Scale(fArrWeights->At(ifile));
      	h2->Add(h2tmp);
        bAdded++;
        }
      else if(fOutput->At(i)->InheritsFrom("TH1")) {
        h1 = dynamic_cast<TH1*>(fOutput->At(i));
	h1tmp = static_cast<TH1*>(list->At(i));
	h1tmp->Sumw2();
        if(fMinStat>0) RemoveLowStatBinsTH1(h1tmp);
	h1tmp->Scale(fArrWeights->At(ifile));
	h1->Add(h1tmp);
        bAdded++;
      } 
      if(bAdded!=1) Printf("bAdded: %d %s",bAdded,fOutput->At(i)->GetName());
    }
    //  list->Clear();
    Printf("weight %d is %e",ifile,fArrWeights->At(ifile));
  }
  CleanMemory();
}

//----------------------------------------------------------------------------
void PtHardBinUtilities::WriteMergedOutput(TString name) {

  TFile *f = new TFile(name.Data(),"RECREATE");

  //  f->WriteTObject(fOutDir,fOutDir->GetName());
  //  fOutDir->cd();
  f->WriteTObject(fOutput,fOutput->GetName());//,TObject::kSingleKey);

  f->Write();
  f->Close();

}

//----------------------------------------------------------------------------
TList *PtHardBinUtilities::GetTList(Int_t ifile) const {

  if(ifile>fListFiles->GetEntries()) return 0x0;
  //  fListFiles->Print();

  TFile *f = (TFile*)fListFiles->At(ifile); 
  //f->ls();
  TList *lst = 0x0;
  if(!fNameHistosDir.IsNull()) {
     TList* firstlist = (TList*)f->Get(fNameHistosDir.Data());
     lst = (TList*)firstlist->FindObject(fNameHistosList.Data());
     
  } else lst = (TList*)f->Get(fNameHistosList.Data());
  Printf("List %s in file %s ", lst->GetName(), f->GetName());
  lst->ls();
  return lst;
}

//----------------------------------------------------------------------------
Bool_t PtHardBinUtilities::InitHistosList() {

  //if(fListHistos) delete fListHistos;

  //  if(!fListHistos) fListHistos = new TList();
  if(fListHistos) 
    fListHistos->Clear();
  else
    fListHistos = new TList();

  TList *histList;
  for(int ifile=0; ifile<fListFiles->GetEntries(); ifile++) {
    Printf("ifile: %d",ifile);
    Printf("%s",fNameHistosDir.Data());
    histList = GetTList(ifile);

    if(!histList) {
      Printf("histList does not exist");
      return kFALSE;
    } else
      fListHistos->Add(histList);
  }
  //fListHistos->Print();
  return kTRUE;
}


//----------------------------------------------------------------------------
TH1* PtHardBinUtilities::GetYProjection(TH2 *h2, Double_t xmin, Double_t xmax) {

  if(!h2) {
    Printf("2D histo does not exist. Returning 0...");
    return 0x0;
  }
  Int_t binMin = h2->GetXaxis()->FindBin(xmin+0.000001);
  Int_t binMax = h2->GetXaxis()->FindBin(xmax-0.000001);
  cout << "Making projection from " << h2->GetXaxis()->GetBinLowEdge(binMin) << " to " << h2->GetXaxis()->GetBinUpEdge(binMax) << endl;

  TH1D *hProj = (TH1D*)h2->ProjectionY("hProj",binMin,binMax);
  return hProj;
}

//----------------------------------------------------------------------------
void PtHardBinUtilities::RemoveLowStatBinsTH1(TH1 *h1) {

  for(int i=1; i<=h1->GetNbinsX(); i++) {
    if(h1->GetBinContent(i)<fMinStat) {
      h1->SetBinContent(i,0);
      h1->SetBinError(i,0);
    }
  }
}

//----------------------------------------------------------------------------
void PtHardBinUtilities::RemoveLowStatBinsTH2(TH2 *h2) {

  for(int i=1; i<=h2->GetNbinsX(); i++) {
    for(int j=1; j<=h2->GetNbinsY(); j++) {
      if(h2->GetBinContent(i,j)<fMinStat) {
        h2->SetBinContent(i,j,0);
        h2->SetBinError(i,j,0);
      }
    }
  }
}

//----------------------------------------------------------------------------
void PtHardBinUtilities::RemoveLowStatBinsTH3(TH3 *h3) {

  for(int i=1; i<=h3->GetNbinsX(); i++) {
    for(int j=1; j<=h3->GetNbinsY(); j++) {
      for(int k=1; k<=h3->GetNbinsZ(); k++) {
        if(h3->GetBinContent(i,j,k)<fMinStat) {
          h3->SetBinContent(i,j,k,0);
          h3->SetBinError(i,j,k,0);
        }
      }
    }
  }
}

//----------------------------------------------------------------------------
void PtHardBinUtilities::RemoveLowStatBinsTHnSparse(THnSparse *hn) {

  // Int_t nDim = hn->GetNdimensions();
  // Double_t* coord = new Double_t[nDim];
  for(int i=1; i<=hn->GetNbins(); i++) { //loop over filled bins
    //   Double_t y = hn->GetBinContent(i,(Int_t*)coord);
    if(hn->GetBinContent(i)<fMinStatHn) {
      // Printf("bin: %d y:%f",i,y);
      // for(Int_t j=0; j<nDim; j++) Printf("coord[%d]=%f",j,coord[j]);
      hn->SetBinContent(i,0);
      hn->SetBinError2(i,0);
    }
  }
  //  delete [] coord;
}

