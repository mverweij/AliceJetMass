#ifndef __PtHardBinUtilities_h__
#define __PtHardBinUtilities_h__
#include "Rtypes.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TH2.h"
#include "TH3.h"
#include "TProfile.h"
#include "THnSparse.h"
#include "TFile.h"
#include "TList.h"
#include "THnSparse.h"
#include "TDirectoryFile.h"

class TGraph;
class TGraphErrors;
class TArrayF;
class TArrayD;

//
// Utility class for output from AliAnalysisTaskJetSpectrum2 task
//

class PtHardBinUtilities {
 public:
  PtHardBinUtilities();
  PtHardBinUtilities(const PtHardBinUtilities& obj); // copy constructor
  //PtHardBinUtilities& operator=(const PtHardBinUtilities& other); // assignment
  virtual ~PtHardBinUtilities() {;}

  void   SetNameWeightList(const char *n)     { fNameWeightList = TString(n) ;}
  void   SetNameHistosDir(const char *n)      { fNameHistosDir  = n          ;}
  void   SetNameHistosList(const char *n)     { fNameHistosList = n          ;}
  void   SetMinStatInBin(Int_t i, Int_t j=-1)  { fMinStat        = i; if(j>-1) fMinStatHn=j; else fMinStatHn=i;}

  void   SetUseDefaultWeights13b4(Bool_t b)   { fUseDefaultWeights13b4 = b   ;}
  void   SetUseDefaultWeights12a15e(Bool_t b) { fUseDefaultWeights12a15e = b ;}

  void   DoWeighting();

  Bool_t InitFileList(TString list = "output.list");
  void   InitWeights();
  Bool_t InitHistosList();
  void   CleanMemory();

  Bool_t SetObjArrayHistoNames();
  Bool_t CreateOutputList();
  Bool_t CreateOutputListDiJetAna();

  void   WriteMergedOutput(TString name = "output.root");
  
  //Getters
  TList   *GetListFiles()    const {return fListFiles;}
  TList   *GetListHistos()   const {return fListHistos;}
  TArrayD *GetArrayWeights() const {return fArrWeights;}
  TArrayF *GetArrayTrials()  const {return fArrTrials;}
  TArrayF *GetArrayXsec()    const {return fArrXsec;}
  TList   *GetTList(Int_t ifile) const;
  TH1     *GetYProjection(TH2* h2, Double_t xmin, Double_t xmax);
  TList   *GetOutputList()   const {return fOutput;}

  void RemoveLowStatBinsTH1(TH1 *h1);
  void RemoveLowStatBinsTH2(TH2 *h2);
  void RemoveLowStatBinsTH3(TH3 *h3);
  void RemoveLowStatBinsTHnSparse(THnSparse *hn);

 protected:
  Bool_t       fUseDefaultWeights13b4;   //use default weight factors for LHC13b4
  Bool_t       fUseDefaultWeights12a15e; //use default weight factors for LHC12a15e
  TFile       *fFileIn;             //output root from Spectrum2 task
  TList       *fListFiles;          //list of root files
  TArrayD     *fArrWeights;         //weight for each pT hard bin
  TArrayF     *fArrXsec;            //cross section for each pT hard bin
  TArrayF     *fArrTrials;          //NTrials for each pT hard bin
  TString      fNameWeightList;     //Name of list which has weights stored
  TString      fNameHistosDir;      //Name of histos dir
  TString      fNameHistosList;     //Name of list with histos to merge
  TList       *fListHistos;         //list of histos TLists
  TObjArray   *fObjArrayHistoNames; //array with names of histost 
  TList       *fOutput;             //!output list with added and weighted histos
  TDirectoryFile *fOutDir;          //!output list with added and weighted histos
  Int_t        fMinStat;            //minimum number of counts in bin
  Int_t        fMinStatHn;          //minimum number of counts in bin of THnSparse
};

#endif
