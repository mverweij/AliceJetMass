#!/bin/bash
# $1=path /alice/sim/2013/LHC13b4_plus/ 
# $2=/PWGJE/Jets_EMC_pPb/142_20130911-1655


BASEDIR=`pwd`

runs=(195531 195567 195568 195593 195644 195673 195783 195831 196085 196310)

for j in `seq 0 9`;
do
    cd $BASEDIR
    mkdir ${runs[$j]}

    for bin in `seq 1 9`;
    do
	[[ ! -d $BASEDIR/${runs[$j]}/0$bin  ]] && mkdir $BASEDIR/${runs[$j]}/0$bin
	cd $BASEDIR/${runs[$j]}/0$bin
	alien_cp alien://$1/${runs[$j]}/$bin/$2/AnalysisResults.root AnalysisResults.root
    done
    
    [[ ! -d $BASEDIR/${runs[$j]}/10  ]] && mkdir $BASEDIR/${runs[$j]}/10
    cd $BASEDIR/${runs[$j]}/10
    alien_cp alien://$1/${runs[$j]}/10/$2/AnalysisResults.root AnalysisResults.root
    
done

    cd $BASEDIR
