\newpage
\section{Background subtraction}\label{ch:BkgSubtraction}

\subsection{$\rho$ and $\rho_{\mathrm{m}}$}\label{sec:RhoRhom}
We employ the method as described in \cite{Soyez:2012hv}. Here background densities are determined by using particles inside several patches within the event. As patches we use \kt{} clusters reconstructed with the FastJet package \cite{Cacciari2011, Cacciari2006}. The transverse momentum density is then given by
\begin{equation}
 \rho = \rom{median} \left\{ \frac{p_{\rom{T},i}}{A_{i}} \right\},
\end{equation}
in which $i$ indicates the $i^{th}$ \kt{} cluster in the event, $p_{\rom{T},i}$ is the transverse momentum of the cluster and $A_i$ is
the area of the \kt{} cluster.
\begin{figure}[!ht]
%\centering
\includegraphics[width=0.48\linewidth]{figures/Rho/RhoChFullTPCVsCent.eps}
\includegraphics[width=0.48\linewidth]{figures/Rho/RhoCh2xEMCalVsCent.eps}
%\hspace{10.00mm}
\includegraphics[width=0.48\linewidth]{figures/Rho/RhoChFullTPCVsNtrack.eps}
\includegraphics[width=0.48\linewidth]{figures/Rho/RhoCh2xEMCalVsNtrack.eps}
\caption{\label{fig:RhoCh}$\rho_{\rom{ch}}$ as function of centrality (upper panels) and uncorrected track multiplicity (lower panels) for two different TPC acceptance selections.
}
\end{figure}
Figure \ref{fig:RhoCh} shows $\rho_{\rom{ch}}$ as function of centrality and number of tracks accepted in the analysis ($|\eta|<0.9$ and $\pttrack>0.15$ \GeVc). The charged background momentum density $\rho_{\rom{ch}}$ is calculated from charged \kt{} clusters with resolution parameter $R=0.2$ which consists of charged tracks while the leading jet in \pt{} is excluded. The background estimate $\rho_{\rom{ch}}$ evolves with the track multiplicity in the event as was also shown in \cite{Abelev:2012ej}. The figures are shown taking the \kt{} clusters within the TPC acceptance ($|\eta|<0.9-R$ and full azimuth) and in a restricted acceptance overlapping with the EMCal ($|\eta|<0.7-R$ and $(0.54+R)<\varphi<(4.01-R)$). The reduced acceptance results in the same mean value but with a larger spread for a fixed centrality or track multiplicity due to the reduced number of \kt{} clusters per event of which $\rho_{\rom{ch}}$ is calculated. 

To take into account the influence of the masses of the jet constituents for each \kt{} cluster a quantity $m_{\delta,k_{\rom{T}}^{\rom{cluster}}}$ is evaluated:
\begin{equation}
 m_{\delta,k_{\rom{T}}^{\rom{cluster}}} = \sum_{j}(\sqrt{m_{\rom{j}}^{2} + p_{\rom{T,j}}^{2}} - p_{\rom{T,j}}),
\end{equation}
where the sum runs over all particles inside the \kt{} cluster, $m_{\rom{j}}$ is the mass and $p_{\rom{T,j}}$ the transverse momentum of each constituent. A second background density $\rho_{\rom{m}}$ for each event is given by
\begin{equation}
 \rho_{\rom{m}} = \rom{median} \left\{ \frac{m_{\delta,i}}{A_{i}} \right\},
\end{equation}
where the subscript $i$ indicates again the $i^{th}$ \kt{} cluster in the event and $A_i$ is the area of the \kt{} cluster. In case all particles are assumed to be massless $\rho_{\rom{m}}$ is by definition zero. In this analysis all measured tracks are assumed to be pions.

Figure \ref{fig:RhoMassCh} shows $\rho_{\rom{m,ch}}$ as function of centrality and number of tracks accepted in the analysis. The magnitude of $\rho_{\rom{m,ch}}$ is much smaller than $\rho_{\rom{ch}}$: for central events $\rho_{\rom{ch}} \sim 140$ \GeVc{} and $\rho_{\rom{m,ch}} \sim 5$ \GeVc. Like for the \pt-density also $\rho_{\rom{m,ch}}$ scales linearly with the track multiplicity.
\begin{figure}[!ht]
%\centering
\includegraphics[width=0.48\linewidth]{figures/Rho/RhoMChFullTPCVsCent.eps}
\includegraphics[width=0.48\linewidth]{figures/Rho/RhoMCh2xEMCalVsCent.eps}
\includegraphics[width=0.48\linewidth]{figures/Rho/RhoMChFullTPCVsNtrack.eps}
\includegraphics[width=0.48\linewidth]{figures/Rho/RhoMCh2xEMCalVsNtrack.eps}
%\hspace{10.00mm}
\caption{\label{fig:RhoMassCh}$\rho_{\rom{m,ch}}$ as function of centrality (upper panels) and uncorrected track multiplicity (lower panels) for two different TPC acceptance selections.
}
\end{figure}

For the charged jet analysis the full TPC acceptance will be used for the calculation of $\rho_{\rom{ch}}$ and $\rho_{\rom{m,ch}}$.

\subsection{Scale factor for full jets}
The measurement presented in this analysis note is based on charged jets. But since in the course of the analysis we also explored the possibility to measure full jets we present in this section how one can obtain the background estimates including the neutral fragments.

In the full jet \PbPb{} spectrum analysis a scale factor is applied to the charged \pt{} density, $\rho_{\rom{ch}}$, to account for the background density given by neutral particles measured in the EMCal. This scale factor is determined using the \pt{} and $E_{\rom{T}}$ in the TPC and EMCal:
\begin{equation}\label{eq:sEMC}
 s_{\rom{EMC}} = C_{\rom{acc}}\dfrac{\sum(E_{\rom{T,cluster}}^{\rom{EMCal}}+p_{\rom{T,track}}^{\rom{EMCal}})}{\sum p_{\rom{T,track}}^{\rom{2 \times EMCal}}},
\end{equation}
with $C_{\rom{acc}} = \rom{TPC_{acc}}/\rom{EMCal_{acc}}$ which is the relative difference between the tracking and EMCal acceptance, and $p_{\rom{T,track}}^{\rom{EMCal}}$ indicates tracks matched to a EMCal cluster.

In this note we propose to calculate the scale factor using \kt{} clusters.
\begin{equation}\label{eq:sNew}
 s = \dfrac{\rho_{\rom{ntr}}^{\rom{EMCal}}+\rho_{\rom{ch}}^{\rom{EMCal}}}{\rho_{\rom{ch}}^{\rom{n \times EMCal}}},
\end{equation}
where the superscripts indicate the acceptance of the \kt{} clusters used to calculated the background densities. The scale factors for $\rho_{\rom{m}}$ and $\rho$ using this method are shown in Fig. \ref{fig:ScaleRho}. The fits, ANA-504 and Train374, are extracted using Eq. \ref{eq:sEMC} for the \pt-density. In the right panel it can be observed that both methods are consistent. On the left hand side the scale factor extracted using Eq. \ref{eq:sNew} for $\rho_{\rom{m}}$ is shown to be similar to the scale factor for $\rho$. The centrality dependence is a bit milder and therefore it is prefered to use Eq. \ref{eq:sNew} for the extraction of the scale factor for $\rho_{\rom{m}}$.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\linewidth]{figures/Rho/RhoScaleFactorFromRhoMass.eps}
\caption{\label{fig:ScaleRho}Scale factor for $\rho_{\rom{m}}$ (left) and $\rho$ (right) as function of centrality.
}
\end{figure}

\newpage
\subsection{Area-based background subtraction}
The area-based subtraction method corrects for background or pileup up effects on jet shapes with a jet-by-jet method \cite{Soyez:2012hv}. The method is valid for any jet algorithm and, infrared and collinear safe jet shapes. The background is chararcterized by $\rho$ and $\rho_{\rom{m}}$ as introduced in Sec. \ref{sec:RhoRhom}. Ghosts are added uniformly in the $y-\varphi$ plane to the event, mimicking a background component in fixed area $A_{g}$. The jet shape is defined by a function of which the derivatives with respect to the transverse momentum scale and a mass component of the ghosts are calculated. Then the value of the jet shape is extrapolated to reach zero background or pileup.

For a complete description of the method see \cite{Soyez:2012hv}.

\subsection{Constituent subtractor}
The constituent method is a particle-level approach which removes or corrects jet constituents \cite{Berta:2014eza}. The particle-by-particle subtraction allows to correct both the 4-momentum of the jet and its substructure. Massless particles with very low momentum, also called `ghosts', are added to the event covering uniformly the $y-\varphi$ plane. Each jet will contain the real particles and also ghosts. A distance measure is defined for each pair of real particle $i$ and ghost $k$:
\begin{equation}
 \Delta R_{i,k} = p_{\rom{T},i}^{\alpha} \cdot \sqrt{(y_{i}-y_{k}^{g})^{2} + (\varphi_{i} - \varphi_{k}^{g})^{2}}.
\end{equation}
An iterative background removal procedure starts from the particle-ghost pair with smallest distance. At each step the transverse momentum and mass of each particle and ghost are modified. For this the background densities $\rho$ and $\rho_{\rom{m}}$ are used to assing a momentum and mass to each ghost: $p_{\rom{T}}^{g}=A_{g}\rho$ and $m_{\delta}^{g}=A_{g}\rho_{\rom{m}}$ where $A_{g}$ is the area of each ghost. If the transverse momentu of particle $i$ is larger than the transverse momentum of the ghost, the ghost is discarded and the transverse momentum from the ghost is subtracted from the real particle. In case the transverse momentum of the ghost is larger than particle $i$, the real particle is discarded and the transverse momentum of the ghost is corrected. The same procedure is applied to the mass of the particles and ghosts. All pairs are considered and the iterative procedure is terminated when the end of the list is reached. The 4-momentum of the jet is recalculated with the same recombination scheme as for the jet finding procedure.

For a complete description of the method see \cite{Berta:2014eza}.
