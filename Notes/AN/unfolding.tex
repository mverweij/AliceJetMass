\newpage
\section{Corrections to particle level}

\subsection{Unfolding in two dimensions}
A four-dimensional response matrix is constructed with the following axises: particle-level \ptjetch{}, detector-level \ptjetch{}, particle-level \mjetch{} and detector-level \mjetch{}. Detector-level jets are obtained from embedding detector-level PYTHIA jets into \PbPb{} events. The embedded detector-level jets are matched to the detector-level jets without \PbPb{} background. The latter are matched to particle-level jets which allows to obtain for each detector-level embedded jet a unique matching to the corresponding particle-level jet. The four-dimensional matrix contains the smearing in jet \pt{} and mass due to background fluctuations and detector effects. The detector-level \ptjetch-axis runs from $-40$ to $140$ \GeVc{} while the particle-level \ptjetch-axis covers the range from $0$ to $150$ \GeVc. The detector-level $M$-axis runs from $-20$ to $40$ \GeVc{} while the particle-level $M$-axis covers the range from $0$ to $40$ \GeVc. 

The four-dimensional response matrix is used to unfold the jet \pt{} and mass simultaneously. The advantage is that you profit from the correlation between the fluctuation on the transverse momentum and mass, see Fig. \ref{fig:DeltaPtDeltaM}. The Bayesian method from RooUnfold \cite{Adye:2011gm} is used for the unfolding. The response matrix encodes a relationship between the jet transverse momentum and mass which are the two variables of interest. PYTHIA is used as a prior and a variation of the prior is applied based on the difference between detector-level embedded jets and data, see section \ref{sec:Syst}.

\subsection{Closure test}
The unfolding is validated by applying the correction procedure to PYTHIA embedded jets. For the signal and the response matrix statistically independent data sets are used. Since there is only one detector simulation available this is achieved by embedding the same PYTHIA jets into different \PbPb{} events. This is results in no correlation between the background fluctuation of the signal PYTHIA jets and those who enter the response matrix. Prior to unfolding, outliers due to the generation in \pt-hard bins are removed. The ratio between the unfolded distribution and the truth can be found in Fig. \ref{fig:UnfTrueMC}. The unfolded and true distribution agree with eachother. Also the refolded distributions agree with the measured.
\begin{figure}[!ht]
 \centering
 \includegraphics[width=\linewidth]{figures/embedding/MCClosure/UnfoldedTrueRatMArea.eps}
 \caption{\label{fig:UnfTrueMC}Ratio between unfolded and true distribution for several \ptjetch{} intervals.
 }
\end{figure}
 
%\subsubsection{Refolded distributions}

\clearpage
\subsection{Data unfolding}

\subsubsection{Comparison to published jet spectrum}
After applying the two-dimensional unfolding procedure one obtains an unfolded distribution of \ptjetch{} versus \mjetch. This result can be used to obtain the unfolded jet \pt-spectrum by projecting to the \ptjetch{} axis. The unfolding is repeated using anti-\kt{} jets with resolution parameter $R=0.3$ to allow comparison to the published result in \cite{Abelev:2013kqa}. The comparison of the unfolded jet \pt-spectrum to the published is shown in Fig. \ref{fig:CompToPubBinWidth5} and \ref{fig:CompToPubBinWidth10}, where the difference is that in Fig. \ref{fig:CompToPubBinWidth5} a bin width of 5 \GeVc{} is used for the \ptjetch{} axis and a bin width of 10 \GeVc{} for Fig. \ref{fig:CompToPubBinWidth10}. The shaded orange boxes show the systematic shape uncertainty of the published result. The first 2--3 iterations are not reliable which can be also observed in the refolded distributions. For a larger number of iterations the unfolded result stabilizes at high \ptjetch{} while for $\ptjetch<60$ \GeVc{} a large variation in 
yield is observed.
\begin{figure}[!ht]
\includegraphics[width=0.45\linewidth]{figures/unfolding/CompToPub/CompToPubSpec.eps}
\includegraphics[width=0.45\linewidth]{figures/unfolding/CompToPub/CompToPubRat.eps}
\caption{\label{fig:CompToPubBinWidth5} Comparison to published result in \cite{Abelev:2013kqa} using 5 \GeVc{} bin width on the jet \pt-axis for the 2D unfolding. Left: jet spectrum for different number of iterations. Right: ratio between jet spectrum from 2D unfolding (this analysis) and published result. The shaded orange area corresponds to the systematic shape uncertainty of the published result. For the comparison the unbiased published spectrum is used. Anti-\kt{} charged jets with $R=0.3$ in 10\% most central \PbPb{} collisions.
}
\end{figure}
%
\begin{figure}[!ht]
\includegraphics[width=0.45\linewidth]{figures/unfolding/CompToPub/CompToPubSpecBinWidth10.eps}
\includegraphics[width=0.45\linewidth]{figures/unfolding/CompToPub/CompToPubRatBinWidth10.eps}
\caption{\label{fig:CompToPubBinWidth10} Comparison to published result in \cite{Abelev:2013kqa} using 10 \GeVc{} bin width on the jet \pt-axis for the 2D unfolding. Left: jet spectrum for different number of iterations. Right: ratio between jet spectrum from 2D unfolding (this analysis) and published result. The shaded orange area corresponds to the systematic shape uncertainty of the published result. For the comparison the unbiased published spectrum is used. Anti-\kt{} charged jets with $R=0.3$ in 10\% most central \PbPb{} collisions.
}
\end{figure}

Figure \ref{fig:MassDistR03} shows the unfolded mass distributions for jets with resolution parameter $R=0.3$. The distributions are normalized per jet. For the central data points 6 iterations in the unfolding are used. The systematic uncertainty (shaded box) indicates the maximum variation of the unfolded distribution for different number of iterations. It can be observed that although the uncertainty on the yield of the \pt-spectrum is large the shape of the mass distributions does not change drastically for $\ptjetch>60$ \GeVc. The largest observed difference is $\approx20$\% and is located in the tail of the distributions for very large jet mass values.
\begin{figure}[!h]
\centering
\includegraphics[width=0.7\linewidth]{figures/unfolding/CompToPub/JetMassDistUnfoldedMethods.eps}
\caption{\label{fig:MassDistR03} Unfolded jet mass distributions for jets with $R=0.3$. The systematic uncertainty (shaded box) indicates the maximum variation of the unfolded distribution for different number of iterations. The nominal number of iterations is 6.
}
\end{figure}

\newpage
\subsubsection{Unfolding with resolution parameter $R=0.4$}
For the final results of this analysis anti-\kt{} jets with resolution parameter $R=0.4$ are used. In this section general features of the unfolding performance are shown. In Sec. \ref{sec:Syst} the systematic uncertainties of the measurement are discussed which includes the uncertainties from the unfolding procedure.

A stability test for the unfolded result is to compare the refolded distribution with the measured. The refolded distribution is obtained by multiplying the unfolded result with the response matrix. The ratio between the refolded and measured distribution is shown in Fig. \ref{fig:RatioMeasFold2D} for unfolding the area-based measurement. Over a very wide \ptjetch{} and jet mass range the refolded distribution reproduces the measured.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\linewidth]{figures/unfolding/iterations/RatioMeasFold2DArea.eps}
\caption{\label{fig:RatioMeasFold2D} Ratio between refolded and measured jet mass (y-axis) versus \ptjetch{} (x-axis) correlation for the area-based method. Each panel is for a diferent number of iterations (upper left: first iteration. Lower right: $15^{\rom{th}}$ iteration).
}
\end{figure}
%
\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\linewidth]{figures/unfolding/iterations/FoldedMArea.eps}
\caption{\label{fig:FoldedMArea} Measured and folded distributions.}
\end{figure}
%
\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\linewidth]{figures/unfolding/iterations/FoldedMeasuredRatMArea.eps}
\caption{\label{fig:FoldedMArea} Ratio between folded and measured distributions.}
\end{figure}
