\clearpage
\section{Systematic uncertainties}\label{sec:Syst}

In this section the systematic uncertainties of the jet mass measurement including the 2D unfolding procedure to particle level are discussed.
The considered sources are:
\begin{itemize}
 \item regularization of unfolding algorithm: number of iterations;
 \item choice of prior;
 \item background subtraction method;
 \item uncertainty on tracking efficiency;
 \item hadronization model;
\end{itemize}
An overview of all systematic uncertainties is given at the end of the section in Fig. \ref{fig:RelDiffSyst}. For the systematic uncertainties from all sources are added in quadrature for the final measurement.

\subsection{Number of iterations}
Figure \ref{fig:MassDistIter} shows the relative difference of the per jet mass distributions for different number of iterations, $N_{\rom{iter}}$, in the unfolding. The relative difference is shown with respect to the $N_{\rom{iter}}$ used for the central data points. For the central data points 6 iterations are used. Especially for large jet masses large variations are observed. The unfolding is more stable for jets with high transverse momentum. For the final result we aim to measure the per jet mass distributions for $\ptjetch>60$ \GeVc. For lower transverse momenta the uncertainty due to the regularization is too large. 
\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\linewidth]{figures/unfolding/iterations/DistMassDiffIterations_Area.eps}
\caption{\label{fig:MassDistIter} Relative difference between nominal unfolded jet mass distribution and unfolded distribution corresponding to a different number of iterations. Prior to taking the ratio the distribution are normalized per jet.
}
\end{figure}
The maximum observed difference in each jet mass bin, excluding the first three iterations, will be assigned as a systematic uncertainty. The systematic uncertainty of from the unfolding itereations is anti-correlated contributing to an uncertainty on the shape of the distribution.

Figure \ref{fig:MeanMassIter} shows the relative difference of the mean jet mass for different number of iterations with respect to the nominal $N_{\rom{iter}}$. For $\ptjetch>60$ \GeVc{} the largest observed deviations are 8\%. The maximum observed difference will be assigned as a systematic uncertainty to the mean jet mass measurement.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{figures/unfolding/iterations/MeanMassDiffIterations_Area.eps}
\caption{\label{fig:MeanMassIter} Relative difference between nominal mean unfolded jet mass and other number of iterations.
}
\end{figure}

\subsection{Prior variation}
To apply a new prior to the response matrix, random values are picked following the distribution of the two-dimensional prior. The smearing from the original response matrix is applied on the \ptjetch{} and \mjetch{} from the prior. This is done by projecting the original response matrix to the \ptjetch-\mjetch{} plane at detector level corresponding to the true values which were picked from the prior. This two-dimensional projection integrates to a total probability of one. A smearing is randomly chosen from the projection at detector level. With these newly obtained smearing values the new response matrix is filled. The procedure is repeated many times conserving the statistics of the original response matrix.

\begin{figure}[!ht]
\includegraphics[width=0.43\linewidth]{figures/unfolding/prior/JetMassPtCompPbPbPYTHIAAreaCent0R040.eps}
\includegraphics[width=0.43\linewidth]{figures/unfolding/prior/JetMassPtCompPbPbPYTHIAAreaCent0R040RMS.eps}
\caption{\label{fig:MeanRMSMPbPbEmb} Comparison mean mass (left) and second moment of mass distribution (right) in \PbPb{} collisions to detector-level embedded PYTHIA jets. Anti-\kt{} charged jets with $R=0.4$ in 10\% most central \PbPb{} collisions.
}
\end{figure}
The variation of the prior is applied based on the difference between detector-level embedded jets and data which is shown in Fig. \ref{fig:MeanRMSMPbPbEmb}. The figure on the left shows the mean jet mass as function of jet \pt{} while the figure on the right shows the second moment (RMS) as function of jet \pt. In the mean a negative shift in the jet mass for \PbPb{} collisions with respect to PYTHIA can be observed. The shape of the distribution however seems to be largely unchanged as shown by the second moment. Therefore it is reasonable to apply a shift on the jet mass to assign a systematic uncertainty to the measurement due to the prior choice.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.45\linewidth]{figures/unfolding/prior/JetMassPtCompPbPbPYTHIAAreaCent0R040Ratio.eps}
\caption{\label{fig:RelDiffMeanMPbPbEmb} Relative difference between mean jet mass in \PbPb{} collisions and embedded detector-level PYTHIA jets. Anti-\kt{} charged jets with $R=0.4$ in 10\% most central \PbPb{} collisions.
}
\end{figure}
Fig. \ref{fig:RelDiffMeanMPbPbEmb} shows the relative difference of the mean jet mass in \PbPb{} collisions and embedded detector-level PYTHIA as function of \ptjetch. A maximum difference of 4\% is observed at large \ptjetch. Based on this observation a variation of $-4\%$ was applied to the PYTHIA prior. The unfolded result with the varied prior is compared to the nominal result in Fig. \ref{fig:AreaVsConst}.
\begin{figure}[!ht]
\centering
%\includegraphics[width=0.7\linewidth]{figures/unfolding/BkgMethod/JetMassDistUnfoldedMethods.eps}
\includegraphics[width=0.7\linewidth]{figures/unfolding/prior/JetMassDistUnfoldedMethodsPriorVar.eps}
\caption{\label{fig:AreaVsConst} Unfolded jet mass distributions for jets with $R=0.4$ for the nominal method (area-based jet shape subtraction and PYTHIA prior) compared to the result using the constituent subtractoin and using the area-based method with a variation of the prior. The systematic uncertainty (shaded box) indicates the maximum variation of the unfolded distribution for different number of iterations. The nominal number of iterations is 6.
}
\end{figure}
%
\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{figures/unfolding/prior/RelSystematicsAll.eps}%RelDiffSyst.eps}
\caption{\label{fig:RelDiffSyst} Systematic uncertainty for jet mass distributions from unfolding iterations, background subtraction method, prior variation and variation of tracking efficiency for each bin in \ptjetch{} and jet mass.
}
\end{figure}
%
\begin{figure}[!ht]
\includegraphics[width=0.47\linewidth]{figures/unfolding/prior/JetMassMeanUnfoldedMethodsPriorVar.eps}
\includegraphics[width=0.47\linewidth]{figures/unfolding/prior/RelMeanSystematicsAll.eps}%RelDiffMeanSyst.eps}
\caption{\label{fig:MeanMassMeth} Left: Mean unfolded jet mass for the nominal method, constituent background subtraction, variation of prior and variation of tracking efficiency. Right: systmatic uncertainty on mean jet mass from different sources.
}
\end{figure}


\subsection{Background subtraction method}
The nominal method for the background subtraction in this analysis is the area-based method. The constituent based method is also used and the result is fully unfolded. The response matrix is obtained by embedding PYTHIA jets and correcting them for the average background using the constituent based method. The fully corrected results from both methods are compared in Fig. \ref{fig:AreaVsConst} and \ref{fig:MeanMassMeth}, where the boxes show the uncertainty from varying the number of iterations in the unfolding. The two methods show a similar result and the difference is included in the systematic uncertainty. The systematic uncertainty of this source is anti-correlated resulting in a shape uncertainty.
%The two methods agree with each other well within the uncertainties of the measurement. Therefore no additional systematic uncertainty is assigned to the measurement.
% \begin{figure}[!ht]
% \centering
% \includegraphics[width=0.7\linewidth]{figures/unfolding/BkgMethod/JetMassMeanUnfoldedMethods.eps}
% \caption{\label{fig:MeanMassMeth} Mean unfolded jet mass for the two background subtraction methods.
% }
% \end{figure}

\clearpage
%\afterpage{\null\newpage}
%\newpage
\subsection{Tracking efficiency}
The effect of the tracking efficiency was studied by changing the tracking efficiency in the detector-level PYTHIA events prior to embedding them into \PbPb{} data. This is achieved by randomly discarding reconstructed tracks in the simulation. A variation of $-3\%$ was applied. The jet mass was found to be insensitive to this uncertainty on the tracking efficiency at detector-level. The data was also unfolded with the response matrix corresponding to the modified tracking efficiency. The comparison between the nominal unfolded distributions and the one using the modified response matrix is shown in Fig. \ref{fig:TrackEffVar}.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{figures/unfolding/TrackEff/JetMassDistUnfoldedMethods.eps}
\caption{\label{fig:TrackEffVar} Comparison between the nominal unfolded distributions and the one using the modified response matrix.
}
\end{figure}

\subsection{Hadronization model}
PYTHIA is used to correct for unmeasured particles, track resolution and efficiency. In addition prior to jet finding the pion mass is assigned to each track. Since not all particles in a jet are pions a correction needs to be applied. This is correction is based on the particle composition inside jets in PYTHIA and is included into the response matrix. No systematic uncertainty due to this correction is assigned. Instead, the result is compared to different event generators in Sec. \ref{sec:DatVsMod}.

\subsection{Total systematic uncertainty}
The contribution of all sources is added in quadrature to obtain the total systematic uncertainty. As can be observed in Fig. \ref{fig:RelDiffSyst} for some jet mass bins the extracted values suffer from fluctuations. The extreme outliers are removed after which a $2^{\rom{nd}}$ order polynomial and gaussian fit is performed, see Fig. \ref{fig:ParamSystTot}. The gaussian fit describes the data points best in all \ptjetch{} bins and will be used to assign the final systematic uncertainty to the measurement.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\linewidth]{figures/unfolding/ParamTotalSyst.eps}
\caption{\label{fig:ParamSystTot} Total systematic uncertainty and parametrization using $2^{\rom{nd}}$ order polynomial and gaussian fit.
}
\end{figure}