\newpage
\section{Results: jet mass in \PbPb{} collisions}\label{sec:results}

\subsection{Detector level jet mass}
In this section the measured jet mass distributions in \PbPb{} collisions are compared to PYTHIA jets. The PYTHIA jets are at detector level and embedded into \PbPb{} data allowing for a direct comparison between data and simulation. Figure \ref{fig:JetMassCompPbPbPYTHIACent0Pt4060} shows the jet mass distributions for jets with $40<\ptjetch<60$ \GeVc. It can be observed that the distributions are similar. A small decrease in the jet mass with respect to PYTHIA can be observed when extracting the mean values as was already shown in Fig. \ref{fig:MeanRMSMPbPbEmb}. 
\begin{figure}[!ht]
\includegraphics[width=0.48\linewidth]{figures/embedding/JetMassCompPbPbPYTHIAEmbeddedAreaCent0PtMin40PtMax60LeadTrkPt0R040.eps}
\includegraphics[width=0.48\linewidth]{figures/embedding/JetMassCompPbPbPYTHIAEmbeddedConstituentCent0PtMin40PtMax60LeadTrkPt0R040.eps}
\includegraphics[width=0.48\linewidth]{figures/embedding/JetMassCompPbPbPYTHIAEmbeddedAreaCent0PtMin40PtMax60LeadTrkPt0R040Ratio.eps}
\includegraphics[width=0.48\linewidth]{figures/embedding/JetMassCompPbPbPYTHIAEmbeddedConstituentCent0PtMin40PtMax60LeadTrkPt0R040Ratio.eps}
\caption{\label{fig:JetMassCompPbPbPYTHIACent0Pt4060}Detector-level jet mass distributions in \PbPb{} data and PYTHIA embedded into \PbPb{} collisions. Centrality: 0-10\%. Anti-\kt{} with $R=0.4$. Left: area-based background subtraction. Right: constituent background subtraction.
}
\end{figure}
The lower panels of Fig. \ref{fig:JetMassCompPbPbPYTHIACent0Pt4060} show the ratio between \PbPb{} and PYTHIA embedded. The ratio decreases as function of jet mass indicating that the nuclear modification factor $R_{\rom{AA}}$ evolves with jet mass.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.45\linewidth]{figures/embedding/JetMeanMassCompPbPbPYTHIAEmbeddedCent0PtMin40PtMax60R040.eps}
\caption{\label{fig:JetMeanMassCompPbPbPYTHIACent0Pt4060}Detector-level mean jet mass in \PbPb{} data and PYTHIA embedded into \PbPb{} collisions. Centrality: 0-10\%. Anti-\kt{} with $R=0.4$.
}
\end{figure}
Comparing the two background subtraction methods, it can be observed that the area-based method gives systematically a larger jet mass than the constituent method. This was observed in the thermal model studies presented in Sec. \ref{sec:ThrmModel}. 
Figure \ref{fig:JetMeanMassCompPbPbPYTHIACent0Pt4060} shows the mean jet mass as function of a leading track requirement for both subtraction methods. When comparing the subtraction from data or embedded PYTHIA for all leading track requirements the area-based method gives a larger mean jet masss. It can also be observed that the data has a smaller jet mass for unbiased jets than PYTHIA but for strongly biased jets (large leading track \pt) the difference vanishes.

\newpage
\subsubsection{Event-plane dependence}
The average jet mass was also studied as function of the azimuthal angular difference between the jet axis and the event-plane. For the estimate of the event-plane the V0 detectors were used, including calibration of the V0 signal. The result at detector-level is shown in Fig. \ref{fig:MeanJetMassVsEP} accompanied with a comparison of the PYTHIA embedded result. Jet production in PYTHIA is not correlated with the event-plane in \PbPb{} collisions. Nevertheless a correlation can be observed which is induced by region-to-region background fluctuations. The magnitude of this correlation can be quantified by looking at the amplitude after fitting a cosine to the data points. This is reported in the figures (`mod') for both \PbPb{} and PYTHIA embedded jets. For jets with $40<\ptjetch<60$ \GeVc{} the modulation is statistically significant larger than the modulation induced by background fluctuations. For higher transverse momentum and more perihperal events (not shown) the current data set lacks statisics for 
extracting a signal. LHC run2 will allow to probe the path-length dependence of the jet mass.
\begin{figure}[!ht]
\includegraphics[width=0.48\linewidth]{figures/embedding/MeanMassVsEPPlusFitCent0PtMin40PtMax60.eps}
\includegraphics[width=0.48\linewidth]{figures/embedding/MeanMassVsEPPlusFitCent0PtMin60PtMax80.eps}
\caption{\label{fig:MeanJetMassVsEP}Detector-level mean jet mass as function of angle with event plane in \PbPb{} data and PYTHIA embedded into \PbPb{} collisions. Centrality: 0-10\%. Anti-\kt{} with $R=0.4$.
}
\end{figure}

\newpage
\subsection{Fully corrected jet mass}
The fully unfolded jet mass distributions including all systematic uncertainties in central \PbPb{} collisions and several \ptjetch{} intervals are shown in Fig. \ref{fig:UnfAllSyst}. The probability of producing jets with a small or large jet mass is small. The peak position of the jet mass distributions increases with \ptjetch. This is further characterized by extracting the mean jet mass as function of \ptjetch{} in Fig. \ref{fig:MeanMassAllSyst}. The mean jet mass increases linearly with jet \pt{} as is also expected from NLO pQCD calculations \cite{Ellis:2007ib}.
\begin{figure}[!ht]
\centering
\includegraphics[width=\linewidth]{figures/results/JetMassDistUnfoldedAllSyst.eps}
\caption{\label{fig:UnfAllSyst} Fully corrected jet mass distribution for anti-\kt{} jets with $R=0.4$ in 10\% most central \PbPb{} collisions.
}
\end{figure}
%
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\linewidth]{figures/results/JetMassMeanUnfoldedAllSyst.eps}
\caption{\label{fig:MeanMassAllSyst} Fully corrected mean jet mass  for anti-\kt{} jets with $R=0.4$ in 10\% most central \PbPb{} collisions.
}
\end{figure}

\subsection{Comparison to models}\label{sec:DatVsMod}
In this section the jet mass measurement is compared to predictions from vacuum and jet quenching event generators.

Figure \ref{fig:PbPbVsPythia} and  shows the measurement in central \PbPb{} collisions together with the PYTHIA vaccum expectation with tune Perugia2011. The \PbPb{} data shows similar trends as PYTHIA but overall the distribution is shifted to smaller jet mass. In Fig. \ref{fig:PbPbVsJewel} the data is compared to the QPYTHIA \cite{Armesto:2009fj} and JEWEL \cite{Zapp:2011ek,Zapp:2012ak} prediction which is are jet quenching Monte-Carlo event generators. JEWEL without keeping track of the recoil shows a smaller jet mass than the vacuum reference. The shift however is larger in JEWEL than the shift observed in the \PbPb{} data. QPYTHIA predicts is strong enhancement of the jet mass within the same \ptjetch{} interval which is attributed is attributed to a strong broadening of the jet profile. Such a broadening is clearly not favored by the \PbPb{} data.
\begin{figure}[!ht]
\centering
\includegraphics[width=\linewidth]{figures/results/JetMassDistUnfoldedWithPythia.eps}
\caption{\label{fig:PbPbVsPythia} Fully corrected jet mass distribution for anti-\kt{} jets with $R=0.4$ in 10\% most central \PbPb{} collisions compared to PYTHIA with tune Perugia2011.
}
\end{figure}
%
\begin{figure}[!ht]
\centering
\includegraphics[width=\linewidth]{figures/results/JetMassDistUnfoldedWithJEWEL.eps}
\caption{\label{fig:PbPbVsJewel} Fully corrected jet mass distribution for anti-\kt{} jets with $R=0.4$ compared to JEWEL predictions for 10\% most central \PbPb{} collisions.
}
\end{figure}

Figure \ref{fig:RatioPbPbToRef} shows the ratio between the jet mass distributions, which are normalized per jet, in central \PbPb{} collisions and the PYTHIA vacuum expectation. This ratio shows how the nuclear modification factor $R_{\rom{AA}}$ evolves with jet mass within a given \ptjetch{} interval. The nuclear modification factor decreases as function of jet mass. This is also the case for JEWEL but in JEWEL the dependence is stronger.
\begin{figure}[!ht]
\centering
\includegraphics[width=\linewidth]{figures/results/RatioToReference.eps}
\caption{\label{fig:RatioPbPbToRef} Ratio between jet mass distribution in \PbPb{} collisions and PYTHIA Perugia2011. Comparison with JEWEL prediction. Here JEWEL without recoil is used. JEWEL is divided by the JEWEL vacuum version.
}
\end{figure}

\clearpage
The mean jet mass in central \PbPb{} collisions and event generators is shown in Fig. \ref{fig:MeanMassPythiaJewel}. The mean jet mass increases both in data and the event generators as function of \ptjetch. JEWEL predicts a significantly lower jet mass than the measurement.
\begin{figure}[!ht]
\includegraphics[width=0.45\linewidth]{figures/results/JetMassMeanUnfoldedAllSystWithPythia.eps}
\includegraphics[width=0.45\linewidth]{figures/results/JetMassMeanUnfoldedAllSystWithJewel.eps}
\caption{\label{fig:MeanMassPythiaJewel} Fully corrected mean jet mass compared to PYTHIA Perugia2011 and JEWEL for anti-\kt{} jets with $R=0.4$ in 10\% most central \PbPb{} collisions.
}
\end{figure}
%
% \begin{figure}[!ht]
% \centering
% \includegraphics[width=0.7\linewidth]{figures/results/JetMassMeanUnfoldedAllSystWithJewel.eps}
% \caption{\label{fig:MeanMassJewel} Fully corrected mean jet mass compared to JEWEL predictions for anti-\kt{} jets with $R=0.4$ in 10\% most central \PbPb{} collisions.
% }
% \end{figure}