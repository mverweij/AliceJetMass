\newpage
\subsection{Embedding in \PbPb{} data}
The performance study of the jet mass reconstruction, as presented in Sec. \ref{sec:ThrmModel}, is repeated by embedding detector-level jets generated with PYTHIA into \PbPb{} data. In this case detector-level embedded jet is the hybrid jet and the detector-level jet from the PYTHIA only event is the probe jet. The matching criteria between the hybrid and probe jet are the same as reported in Sec. \ref{sec:MatchingCrit}. Each detector-level jet also corresponds to a particle-level jet creating a connection between the hybrid embedded jet and the particle-level jet. We study the jet reconstruction performance for two cases:
\begin{enumerate}
 \item Only background fluctuations: comparison between hybrid and detector-level jets;
 \item Background fluctuations and detector effects: comparison between hybrid and particle-level jets;
\end{enumerate}

\subsubsection{Jet mass scale and resolution}
\paragraph{Background fluctuations}
The jet mass scale and resolution due to background fluctuations in data is compared to the thermal model (see Sec. \ref{sec:ThrmModel}). The main difference between embedding in a thermal model and data originates from the real hard jets are already present in the data. There is a finite probability that the PYTHIA jet is embedded on top of a real jet. This will also enter the response and is realistic considering the large amount of binary collisions in a central \PbPb{} event ($N_{\rom{coll}}\simeq1506$). The effect of overlapping jets on the jet \pt-scale is well known: it induces a high \pt{} tail in the \deltapt-distribution \cite{Abelev:2012ej}.

Overlapping jets are also expected to have an effect on the jet mass scale and resolution. The effect is quantified by comparing the jet mass response from embedding PYTHIA jets into thermal model events and \PbPb{} collisions. The relative difference between the reconstructed jet mass, after applying the area-based background subtraction, and the jet mass of the probe jet is shown in Fig. \ref{fig:DeltaMThrmData} for the two embedding cases. The response is shown for different jet \pt{} intervals of the embedded probe. The overlapping jets cause the response to broaden and also the jet mass scale is shifted in positive direction.
\begin{figure}[!ht]
\centering
\includegraphics[width=\linewidth]{figures/embedding/MassShiftDataVsModel.eps}
\caption{\label{fig:DeltaMThrmData} Mass response from jet embedding in thermal model and \PbPb{} collisions. Anti-\kt{} jets with resolution parameter $R=0.4$.
}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\linewidth]{figures/embedding/JetMassFeedOutPtTrue60-80_PtDet60-80.eps}
\caption{\label{fig:DataBkgFluctFeedOut} Effect of feed-out due to \pt-smearing from background fluctuations on jet mass distributions. $60<{\it p}_{\rom{T,probe}}<80$ \GeVc. Anti-\kt{} jets with $R=0.4$. Background subtraction with area-based method.
}
\end{figure}

\paragraph{Detector effects and background fluctuations}
The full response is studied by matching the embedded detector-level jets to the corresponding particle level jet. A comparison of the jet mass response due to background fluctuations to the full response which also contains detector effects is shown in Fig. \ref{fig:DeltaMDetVsPart}. The detector effects are dominated by the tracking efficiency which results in a reduction of the reconstructed jet mass.
\begin{figure}[!h]
\centering
\includegraphics[width=\linewidth]{figures/embedding/Part/MassShiftDetVsPart.eps}
\caption{\label{fig:DeltaMDetVsPart} Mass response for background fluctuations only compared to the full response including detector effects. Anti-\kt{} jets with resolution parameter $R=0.4$.
}
\end{figure}

Fig. \ref{fig:RMSMassVsMTrueDat} shows the jet mass resolution as function of the jet mass at particle level for jets with $60<\ptjetgen<80$ \GeVc. The jet mass resolution strongly depends on the true jet mass as was shown before in Sec. \ref{sec:ThrmModel}.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\linewidth]{figures/embedding/Part/JetMassResolutionVsTrueMassNoFeedInOutPt60-80.eps}
\caption{\label{fig:RMSMassVsMTrueDat} Jet mass resolution taking into account background fluctuations and detector effects as function of $\it{M}_{\rom{probe}}$ for probes with $60<{\it p}_{\rom{T,probe}}<80$ \GeVc. Anti-\kt{} jets with $R=0.4$. Background subtraction is performed with the area-based method.
}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\linewidth]{figures/embedding/Part/JetMassFeedOutPtTrue60-80_PtDet60-80.eps}
\caption{\label{fig:DataBkgFluctDetEffFeedOut} Effect of feed-out due to \pt-smearing from background fluctuations and detector effects on jet mass distributions. $60<{\it p}_{\rom{T,probe}}<80$ \GeVc. Anti-\kt{} jets with $R=0.4$. Background subtraction with area-based method.
}
\end{figure}