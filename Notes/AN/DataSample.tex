\newpage
\section{Data samples, tracks and jets}\label{ch:DataSamples}
This analysis is based on data recorded by ALICE during the fall of 2011 of 
\PbPb{} collisions at $\sNN = 2.76$ TeV. The analysis uses 0-10\% central 
collisions, triggered online by the two forward V0 counters and the Silicon 
Pixel Detector (SPD). The trigger efficiency for hadronic interactions is 100\% 
for events in the 0-7.5\% centrality percentile interval. Pileup is negligible 
in \PbPb{} collision data.

The \pPb{} data sample was collected at the beginning of 2013. The analysis in 
performed using the data taking periods LHC13b, c, d, e (Pb ion going 
in the direction of the V0C side for all these periods). While the first two periods are minimum bias, 
the latter two are jet patches triggered with the EMCal detector (EMCEJE). Two ADC thresholds where used, the lower at 127 and the higher at 260 ADC counts. 

\subsection{Offline event selection}
Events are selected if a primary vertex reconstructed via either global tracks 
or SPD tracklets is present. 
The following selection criteria on the reconstructed vertex are applied:
\begin{itemize}
 \item $|z_{\rom{vtx}}|<10$ cm;
 \item $N_{\rom{contributers}}>2$;
 \item $|z_{\rom{vtx}}^{\rom{SPD}}-z_{\rom{vtx}}^{\rom{TRK}}|<0.1$ cm;
 %\item $z_{\rom{vtx,res}}<0.25$; --> CHECK
\end{itemize}

In addition to the above mentioned event selection, for the \pPb{} analysis the following selection is applied as well:
\begin{itemize}
 \item $z_{\rom{vtx,res}}<0.25$;
 \item pile-up rejection using {\it AliAnalysisUtils::IsPileUpEvent(fAODEvent)};
\end{itemize}


\subsection{Run lists}
The data set is split into a good run list and two semi-good runs lists. For 
the good run list the tracking efficiency is uniform in azimuth $\varphi$. For 
the semi-good run lists a TPC sector caused a reduction of the tracking 
efficiency in specific regions of $\varphi$. Jets overlapping with the 
inefficient regions have not been considered in the analysis. For more 
information see \cite{HJetPbPbANNote}.
\subsection{\PbPb{} run lists}
\paragraph{Good runs}
167902, 167903, 167915, 167920, 167987, 167988, 168066, 168068, 168069, 168076, 
168104, 168107, 168108, 168115, 168212, 168310, 168311, 168322, 168325, 168341, 
168342, 168361, 168362, 168458, 168460, 168461, 168464, 168467, 168511, 168512, 
168777, 168826, 168984, 168988, 168992, 169035, 169091, 169094, 169138, 169143, 
169144, 169145, 169148, 169156, 169160, 169167, 169238, 169411, 169415, 169417, 
169835, 169837, 169838, 169846, 169855, 169858, 169859, 169923, 169956, 170027, 
170036, 170081
\paragraph{Semi-good runs IROC C13}
169975, 169981, 170038, 170040, 170083, 170084, 170085, 170088, 170089, 170091, 
170152, 170155, 170159, 170163, 170193, 170195, 170203, 170204, 170228, 170230, 
170268, 170269, 170270, 170306, 170308, 170309
\paragraph{Semi-good runs OROC C08}
169040, 169044, 169045, 169099, 169418, 169419, 169420, 169475, 169498, 169504, 
169506, 169512, 169515, 169550, 169553, 169554, 169555, 169557, 169584, 169586, 
169587, 169588, 169590, 169591
\subsection{\pPb{} run lists}
The AOD set 154 was used for all periods.
\paragraph{Period b}
195344, 195351, 195389, 195391, 195478, 195479, 195481, 195482, 195483
\paragraph{Period c}
195677, 195675, 195673, 195644, 195635, 195633, 195593, 195592, 195568, 195567, 195566, 195531, 195529, 195596
\paragraph{Period d}
195872, 195871, 195867, 195831, 195829, 195827, 195826, 195787, 195783, 195767, 195760, 195724 
\paragraph{Period e}
196310, 196309, 196308, 196214, 196208, 196201, 196200, 196199, 196197, 196194, 196187, 196185, 196107, 196105, 196099, 196091, 196090, 196089, 196085, 195958, 195955, 195935

\subsection{Detector simulation}
A dedicated jet simulation with full detector transport is used. For \PbPb{} the simulation 
is anchored to runs from the LHC11h data period so that the detector features 
are reflected by the simulation. The period used is LHC12a15e\_fix. PYTHIA tune 
A is used to generate the events. For \pPb{} the reference production is LHC13b4\_plus anchored to LHC13bcde, with Jet-Jet events are simulated with PYTHIA 6, tune Perugia 2011 using 10 $\pT$ hard bins. The two-threshold jet patch triggers are also simulated.

\subsection{Track selection}
For this analysis the so called hybrid tracks are used. Hybrid track selection 
is optimized to recover the tracking acceptance when some parts of the SPD 
ladders are switched off. To ensure uniform distributions in the 
\mbox{($\eta$,$\varphi$)-plane}, an approach of hybrid tracks of the following 
types is used:
\begin{itemize}
 \item global tracks with measured space points in the SPD and a good fit in 
the full ITS
 \item global tracks without any space points in the SPD but with a good fit 
through the remaining ITS layers; track is constrained to the primary vertex
\end{itemize}
When available, tracks of first type with SPD hits are used. Those give the 
best resolution in transverse momentum \pt. Tracks of the second type are 
constrained to the primary vertex of the event to improve the \pttrack{} 
resolution in spite of a missing hit in the SPD. For more information see 
\cite{HJetPbPbANNote}.

\subsection{Jet reconstruction}
Jets are reconstructed with the \kt{} and anti-\kt{} sequential recombination 
jet algorithms as implemented in the FastJet package \cite{Cacciari2011, 
Cacciari2006}. Anti-\kt{} jets are used for the observable of interest while 
\kt{} are used for the background subtraction. Jets with a resolution parameter 
of 0.4 are used. Jets are reconstructed using the E-scheme to recombine the 
4-vectors of the constituents. Jets are reconstructed from charged particles 
within the full azimuthal acceptance and $|\eta|<0.9$. Jets are accepted if 
they are fully contained in the tracking acceptance: full azimuth and 
$|\eta_{\rom{jet}}|<0.5$. For the semi-good runs the areas in $\varphi$ where 
the tracking efficiency is reduced are avoided. For the jet reconstruction a 
ghost area of 0.005 is used to ensure a good jet area resolution.

Each track is assumed to be a pion and has to pion mass assigned to it. Not all 
particles in a jet are in reality a pion and therefore a correction will be 
applied on the inclusive jet sample. Fig. \ref{fig:MassAsumption} shows PYTHIA 
jet mass distribution for different mass assumptions of the constituent. For 
the data points labelled with `Massive', the true mass of the generated PYTHIA 
particle is used. In case on assumes all particles are massless, the jet mass 
is strongly underestimated. If one assigns to all particles the pion mass, as 
is done for the data analysis, the jet mass is still underestimated but the 
effect is much smaller. The last scenario which has been investigated is to 
assign for all pion, kaons and protons the true mass and for all other particles 
the pion mass. In this case only a very small difference in the mean mass is 
observed. It is a bit too large since massless decay particles, like photons 
from a $\pi^{0}$ decay, get the pion mass assigned. Small differences in the 
shape of the distribution are also observed. In case all particles are assumed 
to be pions the distribution is more narrow with respect to the `Massive' case.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.45\linewidth]{figures/JetSelection/MassAssumptionComparison.eps}
\caption{\label{fig:MassAsumption} Jet mass distribution from PYTHIA (Perugia 
2011) for different mass assumption of the constituents.
}
\end{figure}
%\color{red}[INSERT FIGURE SHOWING EFFECT OF MASS ASSUMPTION]\color{black}