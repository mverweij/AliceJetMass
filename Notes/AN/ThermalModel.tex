%\newpage
\section{Performance study}\label{sec:PerfStudy}

\subsection{Thermal model}\label{sec:ThrmModel}
We will study the performance of jet mass reconstruction in heavy-ion collisions by generating background events resembling the particle densities observed at the LHC: thermal model. Soft particle production in the model follows a Boltzmann distribution with $\left< \pt \right>=670$ \MeVc. The hard component is added by embedding PYTHIA events into this soft background providing a data sample referred to as 'hybrid events'. The hard jets in the PYTHIA events without background are the probe jets while the jets reconstructed from the hybrid events are distorted by the background.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\linewidth]{figures/ThrmModel/JetMassComparison.eps}
\caption{\label{fig:JetMassRawCorr}Jet mass for jets with $p_{\rom{T,jet}}^{\rom{probe}}>40$ \GeVc{} in hybrid events compared to probe and background subtracted jets. Anti-\kt{} jets with resolution parameter 0.4 and $|\eta_{\rom{jet}}<0.5|$. Anti-\kt{} jets with $R=0.4$.
}
\end{figure}
Fig. \ref{fig:JetMassRawCorr} shows the mass distribution for probe jets (open black circles) and jets from hybrid events ('Raw', blue solid squares). A large positive mass shift due to the presence of the background is observed. In order to correct for this shift we explore two background subtraction methods which correct the jet momentum vector and the jet energy \cite{Soyez:2012hv,Berta:2014eza}. In both approaches background densities are determined with a data-driven method using particles inside several patches within the event, see Sec. \ref{ch:BkgSubtraction}. The corrected distributions are also shown in Fig. \ref{fig:JetMassRawCorr} in the solid circles. Both subtraction techniques correct for most of the shift in jet mass induced by the background. A small offset remains which is attributed due to regional fluctuations of the background which needs to be corrected inclusively.

\subsubsection{Matching criteria between hybrid and probe jet}\label{sec:MatchingCrit}
Not all constituents of an embedded probe jet will be necessarily found back in one reconstructed heavy-ion jet. In order relate the hybrid to the probe jet a matching condition is used. The algorithm to connect the two jets requires that the hybrid jet carries at least $50$\% of the originally embedded transverse momentum. Requiring at least half of the transverse momentum of the probe jet in the hybrid jet guarantees an unique matching. In case a hybrid jet is paired to two or more probe jets it is matched to the probe jet with highest \pt{} and the other probe jets are considered as lost reducing the jet finding efficiency. The efficiency loss in the 10\% most central events for charged jets increases from 40\% at $\ptjetch=10$ \GeVc{} to a negligible effect at $\ptjetch=40$ \GeVc{} \cite{ChJetPbPbANNote}.

\subsubsection{Correlation between jet \pt{} and mass scale}
Given the strong correlation between jet \pt{} and mass, one also expects that the region-to-region fluctuations are correlated. Region-to-region fluctuations on the \pt-scale are quaracterized by the \deltapt-distribution. \deltapt{} is calculated as the difference between the transverse momentum of the background subtracted hybrid jet and the probe jet \cite{Abelev:2012ej}. Analogous we also calculate \deltaM{}, defined as the difference between the jet mass of the background subtracted hybrid jet and the probe jet.
\begin{figure}[!ht]
\includegraphics[width=0.48\linewidth]{figures/ThrmModel/DeltaPtDeltaMDeriv.eps}
\includegraphics[width=0.48\linewidth]{figures/ThrmModel/DeltaPtDeltaMConst.eps}
\caption{\label{fig:DeltaPtDeltaM}Correlation between \deltapt{} and \deltaM{} for area-based subtraction method (left) and constituent subtractor (right). The response for jets corresponding to a probe with transverse momentum larger than 40 \GeVc{} is shown. Anti-\kt{} jets with $R=0.4$.
}
\end{figure}
Figure \ref{fig:DeltaPtDeltaM} shows the correlation between \deltapt{} and \deltaM{} for both subtraction methods. Hybrid jets were selected if the corresponding probe has a transverse momentum larger than 40 \GeVc. A positive linear correlation between \deltapt{} and \deltaM{} is observed. In case a jet is situated on an upward fluctuation of the background, particles not belonging to the hard jet will contribute to the \pt{} and mass of the jet. The absolute contribution is however not the same: the background fluctuation on \pt{} is larger than on the jet mass.

\subsubsection{Jet mass scale and resolution}
We study mass distributions for several intervals of \ptjet. Figure \ref{fig:MVsPtProbe} shows the mass distribution for the following selections:
\begin{enumerate}
 \item w/o AA bkg: the mass of the probe jet;
 \item w/o fakes, w/o \deltapt: the mass of the hybrid jet, \pt{} selection based on $p_{\rom{T,jet}}^{\rom{probe}}$;
 \item w/o fakes, w/ \deltapt: the mass of the hybrid jet, \pt{} selection based on $p_{\rom{T,jet}}^{\rom{sub}}$ including the effect of the background on the jet \pt-smearing;
 \item w fakes, w/ \deltapt: mass of all jets in hybrid events, including those who do not match to an embedded probe;
\end{enumerate}
\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textheight, angle =90 ]{figures/ThrmModel/MVsPtProbeDerivCent0.eps}
\caption{\label{fig:MVsPtProbe}Jet mass distributions with and without background for several jet \pt{} intervals. Anti-\kt{} jets with $R=0.4$ and area-based background subtraction.
}
\end{figure}
At low transverse momenta (the upper three panels of Fig. \ref{fig:MVsPtProbe}) there is a large fraction of jets in the hybrid events which do not originate from a probe jet indicating that the fraction of fake jets is large. For $\ptjet>50$ \GeVc{} the fake jet contributions is small. The hybrid jets matched to a probe jet, show an offset with respect to the jet mass distribution of the probes. This is partially due to background fluctuations which induce an offset (category 2). The other source of the offset is the limited purity and efficiency within a reconstructed jet \pt-interval. Due to the \pt-smearing, induced by the region-to-region fluctuations of the background, jets migrate from one \pt-interval to another one. This will be discussed in more detail in Sec. \ref{sec:FeedInOut};

Figure \ref{fig:DeltaMPtProbe} shows the relative jet mass residuals for different intervals of the transverse momentum of the probe. The response is asymmetric with a higher probability to reconstruct a too large mass than to underestimate the jet mass.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\linewidth]{figures/ThrmModel/DeltaMPtProbe.eps}
\caption{\label{fig:DeltaMPtProbe}Jet mass residuals as function of $\it{p}_{\rom{T,probe}}$. Anti-\kt{} jets with $R=0.2$.
}
\end{figure}

Figure \ref{fig:MeanRMS} characterizes the response further with on the left the mean of the relative residuals. This characterizes the jet mass scale and is shown for both subtraction methods. With the constituent subtraction the jet mass scale is better than for the area-based subtraction: a jet mass shift of 10\% and 20\% is observed respectively. In the right panel of Fig. \ref{fig:MeanRMS} the width of the relative residuals is shown. This can be interpreted as the resolution of the jet mass. The resolution improves with increases $\it{p}_{\rom{T,probe}}$ and a better performance is observed for the constituent subtracted compared to the area-based subtraction. Overall the resolution is poor and can be further understood by exploring the true jet mass dependence as is shown in Fig. \ref{fig:RMSMassVsMTrue}. A strong dependence on $\it{M}_{\rom{probe}}$ is observed. The resolution for jets with a small mass is poor while for larger jet masses the resolution is 10\%. Jets with small mass are very 
collimated and typically have a small number of constituents. For large enough \ptjet{} ($>40$ \GeVc), jets with a small jet mass are rare (see Fig. \ref{fig:JetMassRawCorr}) and therefore the poor resolution for very collimated jets with small number of constituents is not a limiting factor in this analysis.
\begin{figure}[!ht]
\includegraphics[width=0.48\linewidth]{figures/ThrmModel/MeanVsPtProbeRel.eps}
\includegraphics[width=0.48\linewidth]{figures/ThrmModel/RMSVsPtProbeRel.eps}
\caption{\label{fig:MeanRMS}Relative mean (left) and RMS (right) for jet mass response. Anti-\kt{} jets with $R=0.2$.
}
\end{figure}
%
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\linewidth]{figures/ThrmModel/JetMassResolutionVsTrueMassNoFeedInOutPt40-60.eps}
\caption{\label{fig:RMSMassVsMTrue} Jet mass resolution as function of $\it{M}_{\rom{probe}}$ for probes with $40<{\it p}_{\rom{T,probe}}<60$ \GeVc. Anti-\kt{} jets with $R=0.2$. Background subtraction is performed with the area-based method.
}
\end{figure}

\subsubsection{Feed-in and feed-out due to background fluctuations}\label{sec:FeedInOut}
The fluctuating background induces a \pt-smearing resulting in a mixture of jets when selecting a specific \ptjet-interval. Jets which are measured with a transverse momentum outside of the kinematic interval of interest but have a true \ptjet{} within this same interval are referred to as feed-out. The relative contributions of feed-out are shown in Fig. \ref{fig:ThrmFeedOut} (right panel). About 80\% of the jets is measured within the correct \pt-interval while $10-20$\% migrates to lower \ptjet{} and $2-10$\% migrates to higher \ptjet. The migration to lower \ptjet{} is larger because the jet yield at the low edge of the \pt-interval is highest. It is more likely for jets with small mass to migrate outside of requested interval.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\linewidth]{figures/ThrmModel/JetMassFeedOutPtTrue40-60_PtDet40-60.eps}
\caption{\label{fig:ThrmFeedOut} Effect of feed-out due to \pt-smearing on jet mass distributions. $40<{\it p}_{\rom{T,probe}}<60$ \GeVc. Anti-\kt{} jets with $R=0.2$. Background subtraction with area-based method.
}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.45\linewidth]{figures/ThrmModel/JetMassFeedInPtTrue40-60_PtDet40-60.eps}
\caption{\label{fig:ThrmFeedIn} Effect of feed-in due to \pt-smearing on jet mass distributions. $40<{\it p}_{\rom{T,jet}}<60$ \GeVc. Anti-\kt{} jets with $R=0.2$. Background subtraction with area-based method.
}
\end{figure}
Jets which are measured within the kinematic interval of interest but which have a true \ptjet{} outside of this range contaminate the measurement. These jets are called feed-in. Feed-in from low transverse momentum is the dominating effects. These are jets with a true \ptjet{} smaller than the requested interval but which due to an upward background fluctuation are measured in the interval of interest. This is illustrated in Fig. \ref{fig:ThrmFeedIn}. It can also be observed that the feed-in from low \pt{} increases the average measured jet mass. The true jet mass of these jets is small but it is significantly increased by the large and broad background.

\subsubsection{Sensitivity of mass scale and resolution to fragmentation pattern}
Here we present a comparison of the jet mass reconstruction performance in high density environment for different embedded probes. The embedded probes we consider are PYTHIA anti-\kt{} jets and quenched QPYTHIA anti-\kt{} jets with resolution parameter $R=0.4$. QPYTHIA models jet quenching and exhibits a strong jet broadening resulting in larger jet masses, see Sec. \ref{sec:Models}. Figure \ref{fig:MeanRMSQPYTHIA} shows the response of the QPYTHIA jets to the thermal background through the mean and width of the relative residuals. QPYTHIA is compared to the earlier shown PYTHIA (tune Perugia 2011) and QPYTHIA without a medium ($\hat{q}=0$, DW tune).
\begin{figure}[!ht]
\includegraphics[width=0.48\linewidth]{figures/ThrmModel/QPYTHIA/MeanVsPtProbeArea-basedRel.eps}
\includegraphics[width=0.48\linewidth]{figures/ThrmModel/QPYTHIA/RMSVsPtProbeArea-basedRel.eps}
\includegraphics[width=0.48\linewidth]{figures/ThrmModel/QPYTHIA/MeanVsPtProbeConstituentRel.eps}
\includegraphics[width=0.48\linewidth]{figures/ThrmModel/QPYTHIA/RMSVsPtProbeConstituentRel.eps}
\caption{\label{fig:MeanRMSQPYTHIA}Relative mean (left) and RMS (right) for jet mass response for PYTHIA and QPYTHIA jets. As a reference also QPYTHIA without quenching is shown ($\hat{q}=0$, DW tune). Anti-\kt{} jets with $R=0.4$.
}
\end{figure}

While for PYTHIA jets with $R=0.2$, as shown in Fig. \ref{fig:MeanRMS}, no jet \pt-dependence of the jet mass scale is observed, for the larger jets shown in Fig. \ref{fig:MeanRMSQPYTHIA} a jet \pt-dependence is visible for both subtraction methods. At low jet \pt{} the subtraction is too strong resuulting in a mean jet mass smaller than the probe. At high \ptjet{} a positive offset is observed indicating that the subtracted jet mass is larger than the probe's mass.

The jet \pt-dependence of the jet mass scale is similar for PYTHIA and quenched jets. The absolute value of the shift however is different: a smaller shift for QPYTHIA jets is observed. For all considered jet momenta the subtraction algorithm tends to subtract too much of the QPYTHIA jets, resulting in a too small jet mass.

The resolution of the jet mass, shown in the right panels of Fig. \ref{fig:MeanRMSQPYTHIA}, improves with jet \pt{} and is $8-10$\% better for the QPYTHIA jets. This can be understood by the mass dependence of the resolution shown in Fig. \ref{fig:RMSMassVsMTrue} and that the QPYTHIA jets have a larger mass due to jet broadening.