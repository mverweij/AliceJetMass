#!/bin/sh

echo ""
echo "started pdflatex of AnalysisNoteJetMassPbPb.tex"

pdflatex --shell-escape --synctex=1 AnalysisNoteJetMassPbPb.tex
bibtex AnalysisNoteJetMassPbPb
pdflatex --shell-escape --synctex=1 AnalysisNoteJetMassPbPb.tex

echo "               done!"
