\section{Jet mass in \pPb{} collisions}\label{sec:jetMasspPb}
We apply the same unfolding procedure as in the \PbPb{} analysis in \pPb{} to correct for detector effects. In this case the detector-level axis for jet \pt{} and mass starts at 0 while in the response matrix corresponding to \PbPb{} also negative values are included. The response matrix is extracted from detector simulation by matching detector-level jets to particle-level jets. The unfolding stability is evaluated by comparing the refolded result with the measured distribution. The two distribution are in agreement with each other as is shown in Fig. \ref{fig:FoldedMeasuredRatPPb}.
\begin{figure}[!ht]
\centering
\includegraphics[width=\linewidth]{figures/pPb/FoldedMeasuredRatMArea.eps}
\caption{\label{fig:FoldedMeasuredRatPPb} Ratio between folded and measured jet mass distributions for several \ptjetch{} intervals for \pPb{} data.
}
\end{figure}

The unfolding is performed separately for the minimum-bias and EMCal jet triggered event sample. After unfolding for the final result the minimum bias data is used for $\ptjetch<80$ \GeVc{} while the jet triggered data is used at higher \ptjetch. For the triggered event sample the detector-level \ptjetch{} axis starts at 60 \GeVc. This allows for feed-in from lower \ptjetch{} but avoids including the jets with a large neutral energy fraction bias due to the online EMCal jet trigger. The lower \ptjetch{} was varied to 40 \GeVc{} to test if the effect of feed-in is not underestimated and no significant difference was found.

For the systematic uncertainty we evaluate the sensitivity to the number of chosen unfolding iterations, the background subtraction method and the uncertainty on the tracking efficiency. The unfolding stabilizes at the second iteration and we use the third iteration for the nominal result while the iterations, excluding the first one, are used to assign a systematic uncertainty for the regularization in the unfolding method. The uncertainty on track selection is evaluated by reducing the tracking efficiency at detector level in simulation by 4\%. This provides a new response matrix which is used to repeat the unfolding. The difference between this unfolded result and the nominal is symmetrized and included into the systematic uncertainty of the measurement. The nominal response matrix is also used to correct the jet mass measurement using the constituent subtraction. The difference is also included into the systematic uncertainty. The effect of all these systematic variations in shown for the mean jet mass 
as function of \ptjetch{} in Fig. \ref{fig:MeanMassMethPPb} (left panel). The observed difference from all sources is less than 5\% as can be seen in the right panel of Fig. \ref{fig:MeanMassMethPPb} where the fractional contribution of each source to the systematic uncertainty is shown. All systematic uncertainty are added in quadrature to assign a total systematic uncertainty on the measurement.
\begin{figure}[!ht]
\includegraphics[width=0.47\linewidth]{figures/pPb/JetMassMeanUnfoldedMethodsPriorVar.eps}
\includegraphics[width=0.47\linewidth]{figures/pPb/RelMeanSystematicsAllpPb.eps}
\caption{\label{fig:MeanMassMethPPb} Left: Mean unfolded jet mass for the nominal method, constituent background subtraction, variation of prior and variation of tracking efficiency. The boxes around the data points show the maximum variation from unfolding iterations. Right: systmatic uncertainty on mean jet mass from different sources.
}
\end{figure}

The systematic variations are also evaluated for the jet mass distribution in each \ptjetch{} interval and the fractional contribution of each source is shown in Fig. \ref{fig:RelDiffSystPPb}. Also in this case all systematic uncertainties are added in q	uadrature.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.77\linewidth]{figures/pPb/RelDiffSyst.eps}
\caption{\label{fig:RelDiffSystPPb} Systematic uncertainty for jet mass distributions from unfolding iterations, background subtraction method, prior variation and variation of tracking efficiency for each bin in \ptjetch{} and jet mass.
}
\end{figure}
%

\newpage
\subsection{Background fluctuations in \pPb{} environment}
Although the underlying event in \pPb{} collisions is small, it might still affect the measurement. The measurement is corrected jet-by-jet for the average background. The unfolding procedure in the \pPb{} analysis corrects for detector effects but not for fluctuations of the background. The influence of background fluctuations on the measurement is tested by embedding high \pt{} single particle into the \pPb{} events. In each event a particle with transverse momentum between 40 and 120 \GeVc{} is embedded. The embedded particles are uniformely distributed in the $\eta-\varphi$ plane in the full azimuthal acceptance and $|\eta|<0.5$, corresponding to the jet acceptance for the analysis. The embedded particles are massless and only one particle per event is embedded. After embedding, anti-\kt{} jets are reconstructed and the background subtraction is performed. The background densities are determined from the original \pPb{} event excluding the embedded particle. From all the reconstructed jets only those containing an embedded particle are selected.

Figure \ref{fig:MasspPbEmb} shows the mass distributions of the matched reconstructed jets. On the left for all jets and on the right for those that were contaminated by at least one particle from the \pPb{} event. Taking into account all embedded particles causes a double peak in the jet mass distribution: one peak at negative jet mass and the other peak at positive jet mass. The peak at negative jet mass disappears when at least one background particle is required. The jets formed around the particles embedded in empty regions of the \pPb{} event are still subtracted for the average background in the event resulting in a too small reconstructed jet mass. The reconstructed jets in the active regions of the events are under-subtracted which is to be expected. The embedded particles can be also embedded on top of a real \pPb{} jet. Since the number of binary collisions collisions, the probability for this happening is much smaller in reality than what is achieved by this embedding exercise.
\begin{figure}[!ht]
 \includegraphics[width=0.48\linewidth]{figures/pPb/BkgFluct/cmassNConst1.eps}
 \includegraphics[width=0.48\linewidth]{figures/pPb/BkgFluct/cmassNConst2.eps}
 \caption{\label{fig:MasspPbEmb}Mass difference between reconstructed jet and embedded probe in minimum bias \pPb{} collisions. Left: reconstructed jets containing an embedded particle. Right: only those reconstructed jets containing the embedded particle and at least one particle from the \pPb{} event.}
\end{figure}
%

The effect of the background fluctuations in \pPb{} collisions is negligible for the mean jet mass (see Fig. \ref{fig:FoldedPythiaPPbFluc}). The background fluctuations induce a broadening of the mass distribution. This broadening is overestimated in Fig. \ref{fig:FoldedPythiaPPbFluc} since overlap with true \pPb{} jets are not avoided in extracting the response.
\begin{figure}[!ht]
 \includegraphics[width=\linewidth]{figures/pPb/BkgFluct/FoldedPythiaPPbFluc.eps}
 \caption{\label{fig:FoldedPythiaPPbFluc}PYTHIA particle-level jet mass distributions folded with background response extracted with single track embedding. Black: true PYTHIA. Blue: folded PYTHIA.}
\end{figure}


\newpage
\subsection{Fully corrected jet mass measurement in \pPb{} collisions}
Figures \ref{fig:pPbVsPythiaMean} and \ref{fig:pPbVsPythiaDist} show the fully corrected jet mass measurement together with a PYTHIA prediction at $\sqrt{s}=5.02$ TeV. The measurement and PYTHIA are in agreement.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\linewidth]{figures/pPb/JetMassMeanUnfoldedAllSystWithPythia.eps}
\caption{\label{fig:pPbVsPythiaMean} Fully corrected mean jet mass as function of \ptjetch{} for anti-\kt{} jets with $R=0.4$ in minimum bias \pPb{} collisions compared to PYTHIA with tune Perugia2011.
}
\end{figure}
%
\begin{figure}[!ht]
\centering
\includegraphics[width=\linewidth]{figures/pPb/JetMassDistUnfoldedWithPythia.eps}
\caption{\label{fig:pPbVsPythiaDist} Fully corrected jet mass distribution for anti-\kt{} jets with $R=0.4$ in minimum bias \pPb{} collisions compared to PYTHIA with tune Perugia2011.
}
\end{figure}

