\section{Jet mass as jet quenching observable}\label{ch:JetMass}
The fragmentation process of a high energetic parton propagating through a dense medium, created in a heavy-ion collisions, is potentially modified compared to the parton shower in vacuum. Collimated jets have a small jet mass while jets with a broad profile have a larger jet mass. Interactions with the medium increase the virtuality of the propagating partons, which increases the radiation at large angles from the leading parton \cite{Vitev:2005yg,Renk:2009nz,Renk:2010zx,Armesto:2011ht}. This would result in a broadening of the jet profile and an increase of the jet mass if all radiated gluons are captured within the definition of the jet. It is expected that the leading parton traversing hot QCD matter experiences substantial virtuality (or mass) depletion along with energy loss\cite{Majumder:2014gda}.

In heavy-ion collisions the jet energy scale is distorted by the large heavy-ion background \cite{Abelev:2012ej}. The contribution of particles unrelated to the hard jet pointing in the same direction as the jet needs to be subtracted. For jet structure measurements, including the jet mass, it is crucial to understand the influence of the background to all components of the four-vector of the jet.

We will study the performance of jet mass reconstruction in heavy-ion collisions by generating background events resembling the particle densities observed at the LHC: thermal model. Soft particle production in the model follows a Boltzmann distribution with $\left< \pt \right>=670$ \MeVc. The hard component is added by embedding PYTHIA events into this soft background providing a data sample referred to as 'hybrid events'. The hard jets in the PYTHIA events without background are the probe jets while the jets reconstructed from the hybrid events are distorted by the background. The same study is also performed using the ALICE \PbPb{} data as background.

We use sophisticated data-driven background subtraction schemes to correct the jet mass for the contribution of the underlying event. The data is compared at detector level to a PYTHIA reference. We also explore unfolding techniques for two-dimensional observables. The two-dimensional unfolding allows to correct back to particle level after which we compare the data to models.

\subsection{Model predictions}\label{sec:Models}

\subsubsection{Quark versus gluon jets}
Figure \ref{fig:MJetQuarkGluon} shows the jet mass for quark and gluon jets in PYTHIA\footnote{Thanks to Leticia Cunqueiro for running the simulation}. Gluon jets have typically a larger jet mass since they radiate more gluons in the parton shower. Parton showers in PYTHIA are \pt-ordered while in the HERWIG event generator angular ordering is used. HERWIG gives a larger jet mass as was shown in \cite{ATLAS:2012am,Gallicchio:2012ez}. The ATLAS pp measurement \cite{ATLAS:2012am} shows that PYTHIA underestimates the jet mass while HERWIG overestimates it. 
\begin{figure}[!h]
\centering
\includegraphics[width=\linewidth]{figures/intro/MJetQuarkGluon.eps}
\caption{\label{fig:MJetQuarkGluon} Jet mass for quark and gluon jets in different jet \ptjetch{} intervals. Events were generated with PYTHIA Perugia2011.
}
\end{figure}

\newpage
\subsubsection{Jet quenching models}
Fig. \ref{fig:JEWELJetMassR040} shows the predicted jet mass in two jet quenching models: QPYTHIA \cite{Armesto:2009fj} and JEWEL \cite{Zapp:2011ek,Zapp:2012ak}. QPYTHIA incorporates a strong jet broadening with the jet cone which induces an increase in the jet mass. JEWEL has a lot of large angle radiation which ends up outside of the cone resulting in a smaller jet mass at the jet transverse momentum.
\begin{figure}[!h]
\centering
\includegraphics[width=\linewidth]{figures/intro/JEWELJetMassR040.eps}
\caption{\label{fig:JEWELJetMassR040} Jet mass distribution from JEWEL and QPYTHIA compared to vacuum in different jet \ptjetch{} intervals.
}
\end{figure}
