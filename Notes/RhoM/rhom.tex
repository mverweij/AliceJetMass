\documentclass[a4paper,10pt]{article}

\setlength{\marginparwidth}{0pt}
\setlength{\marginparpush}{0pt}
\setlength{\marginparsep}{0pt}
\setlength{\topmargin}{11pt}
\setlength{\textheight}{640pt}
\setlength{\textwidth}{440pt}
\setlength{\oddsidemargin}{10pt}
\setlength{\evensidemargin}{10pt}
% \setlength{\voffset}{-10pt}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{hyperref}   %% produces clickable links in xdvi
\usepackage{flafter}
\usepackage{amsmath}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{subfigure}
\usepackage{epsfig}
\usepackage{epstopdf}
\usepackage{fancyhdr}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{color}
\usepackage[modulo]{lineno}
\usepackage{array}
\usepackage{soul}
\usepackage{caption}
% \usepackage[shortlabels]{enumitem}
\usepackage{enumitem}
\usepackage[titletoc]{appendix}
\usepackage{booktabs}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{cite}
\usepackage[numbers,sort&compress]{natbib}

\newcommand{\rom}[1]{{\mathrm{#1}}}   % switch to rom in math 
\newcommand{\pt}{\ensuremath{p_\rom{T}}}
\newcommand{\kt}{\ensuremath{k_\rom{T}}}

\newcommand{\PbPb}{Pb--Pb}
\newcommand{\pp}{pp}
\newcommand{\pPb}{p--Pb}
\newcommand{\sNN}{\ensuremath{\sqrt{s_\mathrm{NN}}}}
\newcommand{\GeV}{GeV}
\newcommand{\GeVc}{\text{\GeV/}\ensuremath{c}}
\newcommand{\MeV}{MeV}
\newcommand{\MeVc}{\text{\MeV/}\ensuremath{c}}

\newcommand{\pttrack}{\ensuremath{p_\rom{T,track}}}

%opening
\title{Background estimates for jet masses and structures}
\author{Marta Verweij}

\begin{document}

\maketitle

\begin{abstract}
This note describes the methods used for correcting all components of the jet 4-vector in heavy-ion collisions. These methods give access to jet structures subtracted for the contribution of particle uncorrelated to the hard process originating from the bulk.
\end{abstract}

\section{$\rho$ and $\rho_{\mathrm{m}}$}
We employ the method as described in \cite{Soyez:2012hv}. Here background densities are determined by using particles inside several patches within the event. As patches we use \kt{} clusters reconstructed with the FastJet package \cite{Cacciari2011, Cacciari2006}. The transverse momentum density is then given by
\begin{equation}
 \rho = \rom{median} \left\{ \frac{p_{\rom{T},i}}{A_{i}} \right\},
\end{equation}
in which $i$ indicates the $i^{th}$ \kt{} cluster in the event, $p_{\rom{T},i}$ is the transverse momentum of the cluster and $A_i$ is
the area of the \kt{} cluster.
\begin{figure}[!ht]
%\centering
\includegraphics[width=0.48\linewidth]{figures/RhoChFullTPCVsCent.eps}
\includegraphics[width=0.48\linewidth]{figures/RhoCh2xEMCalVsCent.eps}
%\hspace{10.00mm}
\includegraphics[width=0.48\linewidth]{figures/RhoChFullTPCVsNtrack.eps}
\includegraphics[width=0.48\linewidth]{figures/RhoCh2xEMCalVsNtrack.eps}
\caption{\label{fig:RhoCh}$\rho_{\rom{m}}$ as function of centrality (upper panels) and uncorrected track multiplicity (lower panels) for two different TPC acceptance selections.
}
\end{figure}
Fig. \ref{fig:RhoCh} shows $\rho_{\rom{ch}}$ as function of centrality and number of tracks accepted in the analysis ($|\eta|<0.9$ and $\pttrack>0.15$ \GeVc). The charged background momentum density $\rho_{\rom{ch}}$ is calculated from charged \kt{} clusters with resolution parameter $R=0.2$ which consists of charged tracks while the leading jet in \pt{} is excluded. The background estimate $\rho_{\rom{ch}}$ evolves with the track multiplicity in the event as was also shown in \cite{Abelev:2012ej}. The figures are shown taking the \kt{} clusters within the TPC acceptance ($|\eta|<0.9-R$ and full azimuth) and in a restricted acceptance overlapping with the EMCal ($|\eta|<0.7-R$ and $(0.54+R)<\varphi<(4.01-R)$). The reduced acceptance results in the same mean value but with a larger spread for a fixed centrality or track multiplicity due to the reduced number of \kt{} clusters per event of which $\rho_{\rom{ch}}$ is calculated.

To take into account the influence of the masses of the jet constituents for each \kt{} cluster a quantity $m_{\delta,k_{\rom{T}}^{\rom{cluster}}}$ is evaluated:
\begin{equation}
 m_{\delta,k_{\rom{T}}^{\rom{cluster}}} = \sum_{j}(\sqrt{m_{\rom{j}}^{2} + p_{\rom{T,j}}^{2}} - p_{\rom{T,j}}),
\end{equation}
where the sum runs over all particles inside the \kt{} cluster, $m_{\rom{j}}$ is the mass and $p_{\rom{T,j}}$ the transverse momentum of each constituent. A second background density $\rho_{\rom{m}}$ for each event is given by
\begin{equation}
 \rho_{\rom{m}} = \rom{median} \left\{ \frac{m_{\delta,i}}{A_{i}} \right\},
\end{equation}
where the subscript $i$ indicates again the $i^{th}$ \kt{} cluster in the event and $A_i$ is the area of the \kt{} cluster. In case all particles are assumed to be massless $\rho_{\rom{m}}$ is by definition zero.

\begin{figure}[!ht]
%\centering
\includegraphics[width=0.48\linewidth]{figures/RhoMChFullTPCVsCent.eps}
\includegraphics[width=0.48\linewidth]{figures/RhoMCh2xEMCalVsCent.eps}
\includegraphics[width=0.48\linewidth]{figures/RhoMChFullTPCVsNtrack.eps}
\includegraphics[width=0.48\linewidth]{figures/RhoMCh2xEMCalVsNtrack.eps}
%\hspace{10.00mm}
\caption{\label{fig:RhoMassCh}$\rho_{\rom{m,ch}}$ as function of centrality (upper panels) and uncorrected track multiplicity (lower panels) for two different TPC acceptance selections.
}
\end{figure}

\section{Scale factor}
In the full jet \PbPb{} spectrum analysis a scale factor is used to scale the charged \pt{} density $\rho_{\rom{ch}}$ to account for the background density given by neutral particles measured in the EMCal. This scale factor is determined using the \pt{} and $E_{rom{T}}$ in the TPC and EMCal:
\begin{equation}
 s_{\rom{EMC}} = C_{\rom{acc}}\dfrac{\sum(E_{\rom{T,cluster}}^{\rom{EMCal}}+p_{\rom{T,track}}^{\rom{EMCal}})}{\sum p_{\rom{T,track}}^{\rom{2 \times EMCal}}},
\end{equation}
with $C_{\rom{acc}} = \rom{TPC_{acc}}/\rom{EMCal_{acc}}$ which is the relative difference between the tracking and EMCal acceptance, and $p_{\rom{T,track}}^{\rom{EMCal}}$ indicates tracks matched to a EMCal cluster.

In this note we propose to calculate the scale factor using \kt{} clusters.
\begin{equation}
 s = \dfrac{\rho_{\rom{ntr}}^{\rom{EMCal}}+\rho_{\rom{ch}}^{\rom{EMCal}}}{\rho_{\rom{ch}}^{\rom{n \times EMCal}}},
\end{equation}
where the superscripts indicate the acceptance of the \kt{} clusters used to calculated the background densities.




\bibliographystyle{mybibstyle}%elsarticle-num}%
\bibliography{biblio}

\end{document}
