#!/bin/sh

echo ""
echo "started pdflatex of rhom.tex"

pdflatex --shell-escape --synctex=1 rhom.tex
bibtex rhom
pdflatex --shell-escape --synctex=1 rhom.tex

echo "               done!"
